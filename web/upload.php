<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>File Upload Field Example</title>

    <!-- ** CSS ** -->
    <!-- base library -->
    <link rel="stylesheet" type="text/css" href="js/extjs/resources/css/ext-all.css" />

    <!-- overrides to base library -->

    <!-- page specific -->

    <link rel="stylesheet" type="text/css" href="css/fileuploadfield.css"/>

    <style type=text/css>
        .upload-icon {
            background: url('images/image_add.png') no-repeat 0 0 !important;
        }
        #fi-button-msg {
            border: 2px solid #ccc;
            padding: 5px 10px;
            background: #eee;
            margin: 5px;
            float: left;
        }
    </style>

    <!-- ** Javascript ** -->
    <!-- ExtJS library: base/adapter -->
    <script type="text/javascript" src="js/extjs/adapter/ext/ext-base.js"></script>

    <!-- ExtJS library: all widgets -->
    <script type="text/javascript" src="js/extjs/ext-all.js"></script>

    <!-- overrides to base library -->
    <script type="text/javascript" src="js/FileUploadField.js"></script>

    <!-- page specific -->
    <script type="text/javascript" src="js/persona/persona-upload.js"></script>

</head>

<?php 

//echo "<script language=\"JavaScript\" src =\"ingreso.js\"> </script>"



        echo use_helper('Javascript');
	
        echo javascript_tag("
        
     
        
        
        
        Ext.onReady(PersonaUpload.init, PersonaUpload.);");
?>
<body>

        <div id="fi-form"></div>
    </p>
</body>
</html>
