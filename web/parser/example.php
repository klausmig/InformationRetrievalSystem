<?php
// Test CVS

require_once 'Excel/reader.php';
require_once 'estudio.php';
require_once 'experiencia.php';
require_once 'articulo.php';
require_once 'fachada.php';


function parser($cedula)

{
// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();


// Set output Encoding.
$data->setOutputEncoding('CP1251');

/***
* if you want you can change 'iconv' to mb_convert_encoding:
* $data->setUTFEncoder('mb');
*
**/

/***
* By default rows & cols indeces start with 1
* For change initial index use:
* $data->setRowColOffset(0);
*
**/



/***
*  Some function for formatting output.
* $data->setDefaultFormat('%.2f');
* setDefaultFormat - set format for columns with unknown formatting
*
* $data->setColumnFormat(4, '%.3f');
* setColumnFormat - set format for column (apply only to number fields)
*
**/

//$data->read('Formato_Hoja_de_Vida.xls');
$data->read("hv/HV_$cedula.xls");

//$data->read('D-10.xls');

/*


 $data->sheets[0]['numRows'] - count rows
 $data->sheets[0]['numCols'] - count columns
 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column

 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
    
    $data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
        if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
    $data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
    $data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
    $data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 
*/

error_reporting(E_ALL ^ E_NOTICE);

define('limite',6);


$persona= array(array("TAG"=>"NOMBRE","CAMPO"=>"nombre"), 
                array("TAG"=>"APELLIDOS","CAMPO"=>"apellido"), 
                array("TAG"=>"DIRECCION","CAMPO"=>"direccion"),
                array("TAG"=>"CEDULA","CAMPO"=>"cedula"),
                array("TAG"=>"TIPO DOCUMENTO","CAMPO"=>"tipo_doc"),
                array("TAG"=>"LUGAR EXPEDICION","CAMPO"=>"lugar_expedicion"),
                

                array("TAG"=>"SEXO","CAMPO"=>"sexo"),
                array("TAG"=>"TELEFONO FIJO","CAMPO"=>"telefono_fijo"),
                array("TAG"=>"TELEFONO CELULAR","CAMPO"=>"telefono_celular"),
                array("TAG"=>"DEPARTAMENTO","CAMPO"=>"departamento"),
                array("TAG"=>"CIUDAD","CAMPO"=>"ciudad"),
                array("TAG"=>"E-MAIL","CAMPO"=>"email"),
                array("TAG"=>"PAIS","CAMPO"=>"pais_origen"),
                array("TAG"=>"ESTADO CIVIL","CAMPO"=>"estado_civil"),
                array("TAG"=>"FECHA DE NACIMIENTO","CAMPO"=>"fecha_nacimiento"),
                
               
                );
                

                                
                
$perfil = array(array("TAG"=>"ESTADO PERFIL","CAMPO"=>"estado_perfil"), 
                array("TAG"=>"NIVEL ACADEMICO","CAMPO"=>"nivel_academico"), 
                array("TAG"=>"AREA DE ESTUDIO","CAMPO"=>"area_estudio"),
              //array("TAG"=>"PUNTAJE ECAES","CAMPO"=>"cedula"),
                array("TAG"=>"RESUMEN PERFIL","CAMPO"=>"resumen")               
               
                );                


//HOJA 1                
$personaFila=array("MIN"=>1,"MAX"=>19);
$perfilFila=array("MIN"=>22,"MAX"=>29);
$idiomaFila=array("MIN"=>31,"MAX"=>300);


//$data->sheets[0]['numRows']

if(existeCedula($cedula))
{


$indice=0;

for ($i = $personaFila['MIN']; $i <= $personaFila['MAX']; $i++) 
{
	for ($j = 1; $j <= limite; $j++) 
    {
		$celda=$data->sheets[0]['cells'][$i][$j];
       // echo "  ".$celda." ";
		
		
		if (($celda!="")&&($celda==$persona [$indice]['TAG']))
		{
		    $j++;
            $celdaContigua=$data->sheets[0]['cells'][$i][$j];
            
            if ($celdaContigua!="")
            {
                $persona [$indice]['VALUE']=$celdaContigua;
               
                
                if ($persona [$indice]['CAMPO']=='cedula')
                    $cedula=$celdaContigua;
                    
                $indice ++;
            
            }
            
           
        
        
        }
	}
//	echo "<br>";
	
}
//Corrigo que sea la misma cedula del sistema

$persona [3]['VALUE']=$cedula;

//Formateo la Fecha de nacimiento
$persona [14]['VALUE']=formatFecha($persona [14]['VALUE']);




//echo "<pre>";
//print_r($persona); 

insertarDatos("sipel_persona",$persona,"cedula",$cedula);






//DATOS PERFIL

$indice=0;

for ($i = $perfilFila['MIN']; $i <= $perfilFila['MAX']; $i++) 
{
	for ($j = 1; $j <= limite; $j++) 
    {
		$celda=$data->sheets[0]['cells'][$i][$j];
       // echo "  ".$celda." ";
		
		
		if (($celda!="")&&($celda==$perfil [$indice]['TAG']))
		{
		    $j++;
            $celdaContigua=$data->sheets[0]['cells'][$i][$j];
            
            if ($celdaContigua!="")
            {
                $perfil [$indice]['VALUE']=$celdaContigua;
                $indice ++;
                

            
            }
            
           
        
        
        }
	}
//	echo "<br>";
	
}

//echo "<pre>";
//print_r($perfil);
 
insertarDatos("sipel_perfil",$perfil,"id_perfil",$cedula);


//DATOS IDIOMAS


$indiceTag=0;

for ($i = $idiomaFila['MIN']; $i <= 40; $i++) 
{



		$celda=$data->sheets[0]['cells'][$i][1];
		
	//	print_r("celda ".$celda);
		
		if($celda=="IDIOMA") 
        {
            $indiceTag=$i;
            break;
		
		  
        }
		
		
}

//print_r("INDEICE TAG:".$indiceTag);

for ($i = $indiceTag+1; $i <= $idiomaFila['MAX']; $i++) 
{

		$celda=$data->sheets[0]['cells'][$i][1];

       // echo "  ".$celda." ";
		
		
		if (($celda!=""))
		{
		
    		$idioma= array(array("CAMPO"=>"idioma"), 
                    array("CAMPO"=>"habla"), 
                    array("CAMPO"=>"escribe"),
                    array("CAMPO"=>"lee")               
                   
                    );  
		  
            $nombreIdioma=$data->sheets[0]['cells'][$i][1];
            $habla=$data->sheets[0]['cells'][$i][2];
            $escribe=$data->sheets[0]['cells'][$i][3];
            $lee=$data->sheets[0]['cells'][$i][4];
            
       
            
            $idioma[0]['VALUE']=$nombreIdioma;
                
           
                
            if ($habla!="")
                  $idioma[1]['VALUE']=$habla;
                  
            if ($escribe!="")
                  $idioma[2]['VALUE']=$escribe;   
            
            if ($lee!="")
                  $idioma[3]['VALUE']=$lee; 
                  
                  
          //  echo "<pre>";
           // print_r($idioma); 
                  
            if (($escribe!="")&&($habla!="")&&($lee!=""))
                insertarGeneral("sipel_idioma",$idioma,"id_idioma",$cedula,"idioma");                                                     
                
      
        }
         else break;
        
        if ($celda=="VALORES POSIBLES") break;
	
//	echo "<br>";
	
}

//$educacion=new estudio();
//$educacion->parser($cedula,$data);


$laboral=new experiencia();
$laboral->parser($cedula,$data);


//$paper=new articulo();
//$paper->parser($cedula,$data);


}

}
/*

/*
for ($i = 1; $i <= $data->sheets[1]['numRows']; $i++) {
	for ($j = 1; $j <= $data->sheets[1]['numCols']; $j++) {
		echo " [$i][$j] ".$data->sheets[1]['cells'][$i][$j]." ";
	}
	echo "<br>"; */
	
	

//}


//print_r($data);
//print_r($data->formatRecords);
?>
