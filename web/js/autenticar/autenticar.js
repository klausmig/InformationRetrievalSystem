var autenticacion;
var autenticacionFormPanel;
var autenticacionWindow;
var boxCaptcha;

Ext.BLANK_IMAGE_URL = '../js/extjs/resources/images/default/s.gif';

function autenticar()
      {
        autenticacionFormPanel.getForm().submit(
        {
          method: 'POST',
          formBind: true,
          success: function(form, action)
          {
          
              obj = Ext.util.JSON.decode(action.response.responseText);
            /*  Ext.Msg.show(
              {
                title: 'Error',
                msg: obj.perfil.value,
                buttons: Ext.Msg.OK,
                animEl: 'elId',
                icon: Ext.MessageBox.ERROR
              }); */
              
             // if((obj.perfil=='Egresado')||(obj.perfil=='Monitor'))
             if(obj.perfil.value=='Admin')
                      window.location = SIPEL_URL+'admin.php/principal';
            else
               window.location = SIPEL_URL+'persona.php/principal';
           /* new Ext.ux.window.MessageWindow(
            {
              title: '<h2>Bienvenido</h2>',
              autoDestroy: true,
              autoHeight: true,
              width: 300,
              textPin: 'Clic aqu&iacute; para evitar el cierre',
              textUnpin: 'Clic aqu&iacute; para cerrar',
              autoHide: true,
              help: false,
              plain: true,
              baseCls: 'x-box',
              iconCls: '',
              shadow: false,
              shiftHeader: true,
              origin: {
                pos: "t-t",
                offX: 540,
                offY: 0
              }
            }).show(Ext.getDoc()); */
          },
          failure: function(form, action)
          {
            if (action.failureType == 'server')
            {
              obj = Ext.util.JSON.decode(action.response.responseText);
              Ext.Msg.show(
              {
                title: 'Error',
                msg: obj.errors.reason,
                buttons: Ext.Msg.OK,
                animEl: 'elId',
                icon: Ext.MessageBox.ERROR
              });
            } else
              Ext.Msg.show(
              {
                title: 'Precauci&oacute;n',
                msg: 'No se pudo llevar a cabo el proceso\n'+action.response.responseText,
                buttons: Ext.Msg.OK,
                animEl: 'elId',
                icon: Ext.MessageBox.WARNING
              });
            autenticacionFormPanel.getForm().reset();
          }
        })
      }

var Login = function () 
{

return { 
		
		
		init: function () {
		

  Ext.QuickTips.init();
  Ext.form.Field.prototype.msgTarget = 'side'; // turn on validation errors beside the field globally
  //Ext.util.CSS.swapStyleSheet('theme', '../js/extjs/resources/css/xtheme-silverCherry.css');

  //Ext.log('Hello from the Ext console. This is logged using the Ext.log function.');

  var tipoUsuariosRegistro = [
  ['Egresado'],['Monitor']
  ];

  var comboTipoUsuariosRegistro = new Ext.data.SimpleStore({
  fields: ['opcionesRegistro'],
  data  : tipoUsuariosRegistro
  });
  
      	var tipoIdentificacion = [
    	['Cedula de Ciudadania'],
    	['Tarjeta de Identidad'],
    	['Cedula de Extrangeria']
    	];
    
    	var comboTipoIdentificacion = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoIdentificacion
    	});
    	
//<img width="90px" height="130px" src="images/apis_images/UVlogoP.gif">
  autenticacionWestPanel = new Ext.Panel
  (
    {
      id: 'autenticacionWestPanel',
      region:'west',
      html:'<div align="center" style="margin: 10px; height: 600px; width: 200px;"><div class="tuxTop"></div><div class="tuxMid" style="height: 370px; font-size:12px; font-weight: normal;"><br /><br /><br /><img  width="200px" height="360px"src="../images/logo_login.jpg"></div><div class="tuxFoot"></div></div>'
    }
  );


  autenticacionFormPanel = new Ext.form.FormPanel(
  {
    frame: true,
    region: 'center',
    title:'SIPEL',
    labelAlign: 'right',
    labelWidth: 85,
    url:  'autenticacion/iniciarSesion',
    defaultType: 'textfield',
    monitorValid: true,
    style: 'margin: 30px auto 30px auto;',
    width: 320,
    deferredRender: false,
    items: [
      new Ext.form.FieldSet(
      {
        title: 'Informaci&oacute;n de usuario',
        autoHeight: true,
        defaultType: 'textfield',
        items: [
        {
          fieldLabel: 'Usuario',
          name: 'usuario',
          allowBlank: false,
          blankText: 'Este campo es requerido'
        }, {
          fieldLabel: 'Contrase&ntilde;a',
          name: 'contrasena',
          inputType: 'password',
          allowBlank: false,
          blankText: 'Este campo es requerido',
		  listeners: {
      specialkey: function(f,e){
         if (e.getKey() == e.ENTER) {
		 	autenticar()

         }
      }
	  }
        }]
      })
    ],
	

	  
    
  //  renderTo: 'formulario',
    buttons: [
    {
      text: 'Iniciar sesi&oacute;n',
      formBind: true,
	  handler: autenticar
	  
    }]
  });
  
  
  
  var panelInfoPersonal = {
            columnWidth: 0.49,
            xtype: 'fieldset',
            labelWidth: 80,
            title:'Informacion Personal',
            defaultType: 'textfield',
            height : 340,
	        width: 100,
//             autoWidth : true,
            bodyStyle: Ext.isIE ? 'padding:0 0 24px 15px;' : 'padding:24px 15px;',
            border: true,
            style: {
              "margin-left": "10px",
            },
            items: [{
              xtype:  'panel',
              bodyStyle:'background:#fFfFfF;',

              height: 30,
              border: false,
              items:
              [{
                xtype: 'label',
                text:  'Los campos con (*) no son obligatorios',
                style: 'font-size:8.5pt; color:#484848;font-weight: bold;',
             
              }]
              },{
                fieldLabel: 'Nombre',
                name: 'nombre',
                id: 'nombre',
                width: 140,
                allowBlank:false,
                blankText: 'El Nombre es obligatorio'
              },{
                fieldLabel: 'Apellido',
                name: 'apellido',
                id: 'apellido',
                allowBlank:false,
                width: 140,
                blankText: 'El Apellido es obligatorio'
              },
                    {
                xtype:      'radiogroup',
                fieldLabel: 'Sexo',
                //style:'background:#f3f3f3;',
               
                items: 
                [{
                  boxLabel:   'F',
                  name:       'Sexo2',
                  id:         'sexo_f_registro',
                  inputValue: 'f'
                  
                },
                {
                  boxLabel:   'M',
                  name:       'Sexo2',
                  id:         'sexo_m_registro',
                  inputValue: 'm',
                }]
          
              },{
                fieldLabel: 'Identificación',
                name: 'Cedula',
                id: 'cedula',
                allowBlank:false,
                width: 140,
                blankText: 'La Cedula es obligatoria'
              },{
				xtype:      'combo',
				fieldLabel: 'Tipo ident.',
				id:         'tipo_identificacion',
				name:'tipo_identificacion',
				store:comboTipoIdentificacion,
				mode:'local',
				displayField: 'opciones',
				triggerAction: 'all',
			    emptyText: 'Selecione....',
				selectOnFocus: true,
				width: 140

					},{
                fieldLabel: 'Teléfono',
                name: 'Telefono',
                id: 'telefono',
                allowBlank:false,
                width: 140,
                blankText: 'El Telefono es obligatorio'
              },{
                fieldLabel: 'Celular(*)',
                name: 'Celular',
                id: 'celular',
                allowBlank:true,
                width: 140,
                blankText: 'El Telefono es obligatorio'
              },{
                fieldLabel: 'Email(*)',
                name: 'Email',
                id: 'email',
                vtype:  'email',
                allowBlank:true,
                width: 140,
                minLength : 5,
                maxLength : 30,
                blankText: 'El Email es obligatorio'
              }
            ]};


//********************************//
ventanaRegistroUsuario = Ext.extend(Ext.Window,{
  onActionFailed : function(f,a){
      this.onCapthaChange();
      var form = this.loginPanel.getForm();
      alert(a.result.errors.msg);
      if (a.result.errors.id){
        var f = form.findField(a.result.errors.id);
        if(f){
          f.focus();
        }
      }
      //Do Action when Login Failed
  },

  onCapthaChange : function(){

	Ext.Ajax.request({
        waitMsg: 'Por Favor Espere...',
          url: 'autenticacion/imagen',
        method: 'GET',
        params: {
        },
        success: function(response, action){
	cadena = response.responseText;
	var captchaURL = "../images/captcha/CaptchaSecurityImages.php?width=160&height=80&characters=4&cadena="+cadena+"&t=";
        var curr = Ext.get('activateCodeImg');
        curr.slideOut('b', {callback: function(){
              Ext.get( 'activateCodeImg' ).dom.src= captchaURL+new Date().getTime();
              curr.slideIn('t');
        }},this); 
        },
        failure: function(action, response){
          var result=response.responseText;
          if(action.failureType == 'server')
          {
            Ext.Msg.show
            ({
              title:'Error',
              msg: 'No se pudo conectar con la base de datos',
              buttons: Ext.Msg.OK,
              animEl: 'elId',
              icon: Ext.MessageBox.ERROR
            });
          }
        }
      });
  },
    initComponent:function()
    {
//       Ext.BLANK_IMAGE_URL = '../../css/extjs/resources/images/default/s.gif';
//       Ext.util.CSS.swapStyleSheet('theme','../../css/extjs/resources/css/xtheme-slate.css');	
       
                                
      this.captchaURL = "../images/captcha/CaptchaSecurityImages.php?width=160&height=80&characters=4&cadena="+cadena+"&t=";
        var boxCaptcha =  new Ext.BoxComponent({
                  columnWidth:.35,
                  autoEl: {
                    tag:'img'
                    ,id: 'activateCodeImg'
                    ,title : 'Click para actualizar la imagen'
                    ,src:this.captchaURL+new Date().getTime()
                  }
                  ,listeners : {
                      'click' : function () {
                        alert('test');
                      }
                    }
        });
        boxCaptcha.on('render',function (){
          var curr = Ext.get('activateCodeImg');
          curr.on('click',this.onCapthaChange,this);
        },this);

      Ext.apply(this,{
      xtype:  'form',
      name: 'formRegistroUsuario',
      id:  'formRegistroUsuario',
      frame: true,
      modal: true,
     bodyStyle:'background:#fFfFfF;',
      deferredRender:false,
      layout: 'column',
      title: 'Registrarse',
      width: 648,
      height: 400,
      items: [panelInfoPersonal,{
            columnWidth: 0.498,
            layout:'form',
            xtype: 'fieldset',
            labelWidth: 90,
            title:'Detalles del sistema',
            defaultType: 'textfield',
            height : 340,
	     width: 100,
//             autoWidth : true,
            bodyStyle: Ext.isIE ? 'padding:0 0 24px 15px;' : 'padding:35px 15px;',
            border: true,
            style: {
              "margin-left": "10px",
            },
            items: [{
                fieldLabel: 'Usuario',
                name: 'login',
                id: 'login',
                allowBlank:false,
                width: 140,
                blankText: 'El Login es obligatorio'
              },{
                fieldLabel: 'Contraseña',
                name: 'Contrasena',
                id: 'clave',
                inputType:'password',
                allowBlank:false,
                width: 140,
                blankText: 'La Contraseña es obligatoria'
              },{
                fieldLabel: 'ReContraseña',
                name: 'ReContrasena',
                id: 'reclave',
                initialPassField: 'usu_contrasena',
                inputType:'password',
                allowBlank:false,
                width: 140,
                blankText: 'La ReContraseña es obligatoria'
              },{
                xtype: 'combo',
                store: comboTipoUsuariosRegistro,
                name: 'tipo_usuario',
                id: 'tipo_usuario',
                mode:'local',
                displayField: 'opcionesRegistro',
                triggerAction: 'all',
                emptyText: 'Selecione....',
                selectOnFocus: true,
                width: 140,
                fieldLabel: 'Tipo Usuario',
              },{
              xtype:'panel',
              border:false,
              height:15
              
              },
              
              {
		    xtype:'fieldset',
		    title:'Código de Seguridad',
		    width: 235,
		    //height:85,
		    defaultType: 'textfield',
		   // style: 'position: absolute; margin-left:35px; top:180px;',
		    items:
		    [boxCaptcha]
		},{
                fieldLabel: 'Código',
                name: 'IDusuarios',
                id: 'idCaptcha',
                allowBlank:false,
                width: 140,
                blankText: 'El codigo del captcha es obligatorio'
              }]
          }],
          buttons: [{
            text: 'Registrar',
            id     : 'btnAccept1_registro',
            iconCls:  'salvar',
            align:'center',
            handler  : function()
            {
		var registraValidada = verificaCamposRegistro();
		if(registraValidada == 'almacenar')
		{
			if(Ext.getCmp('sexo_f_registro').getValue() == true)
			{
			  tipoSexo = 'f';
			}
			if(Ext.getCmp('sexo_m_registro').getValue() == true)
			{
			  tipoSexo = 'm';
			}
			Ext.Ajax.request({
				url: "autenticacion/create",
				params: { 
					task: "CREATEPRES",
					nombre: Ext.getCmp('nombre').getValue(),
					apellido: Ext.getCmp('apellido').getValue(),
					sexo: tipoSexo,
					cedula: Ext.getCmp('cedula').getValue(),
					tipo_usuario: Ext.getCmp('tipo_usuario').getValue(),
					email: Ext.getCmp('email').getValue(),
					telefono: Ext.getCmp('telefono').getValue(),
					tipo_identificacion: Ext.getCmp('tipo_identificacion').getValue(),
					celular: Ext.getCmp('celular').getValue(),
					login: Ext.getCmp('login').getValue(),
					clave: Ext.getCmp('clave').getValue(),
					estado: "1",
				},
				success: function(response, action){
					obj = Ext.util.JSON.decode(response.responseText);
					if (obj.success)
					{
						Ext.Msg.show
						({
							title:'Alerta!',
							msg: obj.mensaje,
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
						ventanaRegistroUsuario1.close();
					}
					else if (obj.errors)
					{
						Ext.MessageBox.show
						({
							title: 'Advertencia',
							msg: obj.errors.reason,
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.WARNING
						});ventanaRegistroUsuario
						ventanaRegistroUsuario1.close();
					}
				},
				failure: function(action, response){
					var result=response.responseText;
					if(action.failureType == 'server')
					{
						Ext.Msg.show
						({
							title:'Error',
							msg: 'No se pudo conectar con la base de datos',
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.ERROR
						});
						ventanaRegistroUsuario1.close();
					}
				}
			});
		}
            }
            },{
            text     : 'Cancelar',
            iconCls:  'cancelar',
            align:'center',
            handler  : function (){ventanaRegistroUsuario1.close();}
            }]
              });
              ventanaRegistroUsuario.superclass.initComponent.apply(this,arguments);
            }
  });

	function verificaCamposRegistro()
	{
		salida =  'almacenar';
		

		if(Ext.getCmp('nombre').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un nombre', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('nombre').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('nombre').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre no debe tener caracteres númericos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un apellido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('apellido').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido no debe tener caracteres númericos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('sexo_m_registro').checked == false && Ext.getCmp('sexo_f_registro').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione un Sexo', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir el número de cédula', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue().length < 8 || Ext.getCmp('cedula').getValue().length > 10 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La cédula debe estar en un rango de 8 a 10 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('cedula').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La cédula debe ser númerica', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('tipo_identificacion').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de identificaion', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('tipo_identificacion').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El tipo no debe tener caracteres númericos ', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('telefono').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un télefono', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('telefono').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El télefono debe ser númerico', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un usuario', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue().length < 5 && Ext.getCmp('login').getValue().length > 20 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El usuario debe estar en un rango de 5 a 20 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() != Ext.getCmp('reclave').getValue()) 
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La contraseña y su confirmación, no coinciden', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir una contraseña', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if((Ext.getCmp('tipo_usuario').getValue() != 'Egresado')&&(Ext.getCmp('tipo_usuario').getValue() != 'Monitor'))
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de usuario válido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else	
        if(Ext.getCmp('idCaptcha').getValue() != cadena)
		{
			Ext.MessageBox.show({title: 'Advertencia', msg: 'El codigo que introdujo en el captcha no es igual al de la imagen', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon: Ext.MessageBox.WARNING });
			return false;
		}
					
	
		
	return salida;
	}
/********************************************/

  var contenedor = new Ext.Panel({
	  region:'center',
	  deferredRender:false,
	  autoScroll: true, 
	  bodyStyle: 'background-color:#EFEFEF; padding:0px 20px 0px 20px;',
	  items:[autenticacionFormPanel,
	    {
	      xtype:'fieldset',
	      title: 'Informaci&oacute;n General',
	      autoWidth: true,
	      height:100,
	      bodyStyle: 'background-color:#EFEFEF; padding:5px 10px 200px 10px; margin: 20px',
	      items :[
	      {
		id:'',
		xtype : 'form',
		border : false,
		frame : false,
		bodyStyle: 'background-color:#EFEFEF; padding:5px 0px 0px 0px;',
		items : [
		{
		    xtype:'label',
		    text: 'SIPEL o más conocido como Sistema de Gestión de Perfiles Laborales, permite la administración de las hojas de vida de los Egresados y Monitores de la Escuela de Ingeniería de Sistemas.',
		    style : 'font-size: 13px; font-family: Arial, Verdana, Helvetica, sans-serif;',
		    labelStyle: 'font-size: 13px; font-weight: bold;'
		}
		]
    ,
    
    buttons:[
		{ 
		    text:'Registro',
		     iconCls:  'registrar',

		    handler:function()
		    { 
                Ext.Ajax.request({
                        waitMsg: 'Por Favor Espere...',
                        url: 'autenticacion/imagen',
                        method: 'GET',
                        params: {
                        },
                        success: function(response, action)
                        {
                            cadena = response.responseText;
                            
         
           
                                
                            ventanaRegistroUsuario1 = new ventanaRegistroUsuario();
                            ventanaRegistroUsuario1.show();
                        },
                        failure: function(action, response)
                        {
                              var result=response.responseText;
                              if(action.failureType == 'server')
                              {
                                Ext.Msg.show
                                ({
                                  title:'Error',
                                  msg: 'No se pudo conectar con la base de datos',
                                  buttons: Ext.Msg.OK,
                                  animEl: 'elId',
                                  icon: Ext.MessageBox.ERROR
                                });
                              }
                        }
                      });
		    }
		}] 
	      } 
	      ]
	}, 
  { 
		  
		id:'',
		xtype : 'form',
		border : false,
		frame : false,
		bodyStyle: 'background-color:#EFEFEF; padding:5px 0px 0px 110px;',
		items : [
		{
		    xtype:'label',
		    text: 'Escuela de Ingeniería de Sistemas y Computación',
		    style : 'font-size: 13px; font-family: Arial, Verdana, Helvetica, sans-serif;',
		    labelStyle: 'font-size: 13px; font-weight: bold;'
		}
		]

	      },
	      {  
		  
		id:'',
		xtype : 'form',
		border : false,
		frame : false,
		bodyStyle: 'background-color:#EFEFEF; padding:5px 0px 0px 130px;',
		items : [
		{
		    xtype:'label',
		    text: 'Centro de Desarrollo de Software (Cedesoft)',
		    style : 'font-size: 13px; font-family: Arial, Verdana, Helvetica, sans-serif;',
		    labelStyle: 'font-size: 13px; font-weight: bold;'
		}
		]
		
	      }]
    });

  autenticacionWindow = new Ext.Window(
  {
    id: 'autenticacionWindow',
    layout: 'border',
    width: 800,
    height: 600,
//     autoHeight: true,
    closable: false,
    resizable: false,
    border: true,
    draggable: false,
    items: [ contenedor, autenticacionWestPanel],
   // renderTo: 'formulario'
  });

  autenticacionWindow.show();



}
  }
}();
