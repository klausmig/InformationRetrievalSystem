// VARIABLE GLOBAL

var __URL_SIPI__ = 'http://thot/web/';
// var _ID_USUARIO_ = '';
// var _ID_PROYECTO_ = '';
// var _ROL_USUARIO_ = '';

var formProductos = true;

function addTab(tabTitle, targetUrl){
		var tabPanel = Ext.getCmp('tabPanel');
		var tab = tabPanel.getItem(tabTitle);
		if(tab)	{
			tabPanel.setActiveTab(tab);
		} else {
			tabPanel.add
			(
				{
					id: tabTitle,
					title: tabTitle,
					iconCls: tabTitle,
					frame: true,
					autoLoad: {url: targetUrl,scripts: true, scope: this},
					closable:true,
					autoScroll: true
				}
			).show();
		}
}

function removeTab(tabTitle){
		var tabPanel = Ext.getCmp('tabPanel');
		var tab = tabPanel.getItem(tabTitle);
		if(tab)	{
			tabPanel.remove(tab);
		} else {
			return true;
		}
}
/*
	tabTitle: String
	targetUrl: String
	paramsObj: objeto de la forma {campo1: valor, campo3: valor, campoN: valor}
*/
function addParamTab(tabTitle, targetUrl, paramsObj){
		var tabPanel = Ext.getCmp('tabPanel');
		var tab = tabPanel.getItem(tabTitle);
		if(tab)	{
			tabPanel.setActiveTab(tab);
		} else {
			tabPanel.add
			(
				{
					id: tabTitle,
					title: tabTitle,
					iconCls: tabTitle,
					frame: true,
					autoLoad: {url: targetUrl,scripts: true, scope: this, params: paramsObj},
					closable:true,
					autoScroll: true
				}
			).show();
		}
}

function verificarFormularioProductos(){
	var toReturn = false;
	if (Ext.getCmp('prod_descripcion').getValue() == ''){
		toReturn = false;
		Ext.Msg.alert('Alerta!', 
						'Digite el nombre o descripci&oacute;n del producto');
	} else if (Ext.getCmp('prod_nombre_proyecto').getValue() == '') {
		toReturn = false;
		Ext.Msg.alert('Alerta!', 
						'Debe tener al menos un proyecto activo');
	} else {
		toReturn = true;
	}
	
	return toReturn;
}

function verificarFormularioMetas(){
	var toReturn = false;
	if (Ext.getCmp('meta_descripcion').getValue() == ''){
		toReturn = false;
		Ext.Msg.alert('Alerta!', 
						'Digite el nombre o descripci&oacute;n de la meta');
	} else if (Ext.getCmp('prod_nombre_proyecto').getValue() == '') {
		toReturn = false;
		Ext.Msg.alert('Alerta!', 
						'Debe tener al menos un producto activo');
	} else {
		toReturn = true;
	}
	
	return toReturn;
}


function verificarFormularioActividades(){
// 	var toReturn = false;
// 	if (Ext.getCmp('acti_descripcion').getValue() == ''){
// 		toReturn = false;
// 		Ext.Msg.alert('Alerta!', 
// 						'Digite el nombre o descripci&oacute;n de la actividad');
// 	} else if (Ext.getCmp('prod_nombre_proyecto').getValue() == '') {
// 		toReturn = false;
// 		Ext.Msg.alert('Alerta!', 
// 						'Debe tener al menos una meta activa');
// 	} else {
// 		toReturn = true;
// 	}
	
	//FIXME: adicionr verificacion de fechas
	return true;
}

function activarCamposProductos(){
	Ext.getCmp('prod_descripcion').enable();
	Ext.getCmp('prod_nombre_proyecto').enable();
}

function activarCamposMetas(){
	Ext.getCmp('meta_descripcion').enable();
	Ext.getCmp('meta_nombre_prod').enable();
	
}

function activarCamposProyecto(){
	Ext.getCmp('prod_nombre_proyecto').enable();	
}

function activarCamposActividades(){
	Ext.getCmp('acti_descripcion').enable();	
	//Ext.getCmp('acti_nombre_meta').enable();
	Ext.getCmp('acti_fecha_ini').enable();
	Ext.getCmp('acti_fecha_fin').enable();
}