



var user = function () 
{

return { 
		init: function () {
		
    	var UsuariosDataStore;
    	var UsuariosColumnModel;
    	var UsuariosListingEditorGrid;
    	var UsuariosListingWindow;
    	var usuariosHabilitados=true;
    	var PerfilesDataStore;
 
        Ext.form.VTypes.cedulaVal  = /^[0-9]+/;
		Ext.form.VTypes.cedulaMask = /[0-9]/;
		Ext.form.VTypes.cedulaText = 'C�dula Invalida.';
		Ext.form.VTypes.cedula 	= function(v){
			return Ext.form.VTypes.cedulaVal.test(v);
		};
             
	

    	UsuariosDataStore = new Ext.data.JsonStore({
    		id: 'UsuariosDataStore',
    		url:'usuarioPersona/cargar',
    		root: 'results',
    		baseParams:{task:'LISTING'},
    		totalProperty: 'total',
    		fields:[
			{name: 'id', type: 'int'},
			{name: 'cedula', type: 'string'},
			{name: 'tipo_identificacion', type: 'string'},
			{name: 'nombre', type: 'string'},
			{name: 'apellido', type: 'string'},
			{name: 'sexo', type: 'string'},
			{name: 'telefono', type: 'string'},
			{name: 'celular', type: 'string'},
			{name: 'tipo_usuario', type: 'string'},
			{name: 'email', type: 'string'},
			{name: 'login', type: 'string'},
			{name: 'clave', type: 'string'},
			{name: 'estado', type: 'int'}
    		],
    		sortInfo:{field: 'cedula', direction: 'ASC'}
    	});
    	UsuariosDataStore.load({params: {start: 0, limit: 10}});
    
      
    	UsuariosColumnModel = new Ext.grid.ColumnModel(
    		[{
    			header: 'cedula',
    			dataIndex: 'cedula',
    			sortable:true,
    			width: 105
    		},{
    			header: 'Nombres',
    			dataIndex: 'nombre',
    			sortable:true,
    			width: 105
    		},{
    			header: 'Apellidos',
    			dataIndex: 'apellido',
    			sortable:true,
    			width: 105
    		},{
    			header: 'Login',
    			dataIndex: 'login',
    			sortable:true,
    			width: 100
    		}
    	]);
    	
    	UsuariosColumnModel.defaultSortable= true;
    
    	UsuariosListingEditorGrid =  new Ext.grid.EditorGridPanel({
    		id: 'UsuariosListingEditorGrid',
    		store: UsuariosDataStore,
    		cm: UsuariosColumnModel,
    		enableColLock:true,
    		clicksToEdit:1,
    	
    		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
    	});
    
    	UsuariosListingWindow = new Ext.Panel({
    		id: 'UsuariosListingWindow',
    		title: 'Informacion de Usuarios',
    		closable:true,
    		width:100,
    		height:390,
    		plain:true,
    		layout: 'fit',
    		frame: true,
    		items: UsuariosListingEditorGrid
    	});



/*	PerfilesDataStore = new Ext.data.JsonStore({
		id: 'UsuariosDataStore',
		url:'usuarios/cargar',
		root: 'results',
		baseParams:{task:'LISTINGPERFIL'},
		totalProperty: 'total',
		fields:[
			{name: 'perId', type: 'string'},
			{name: 'perNombre', type: 'string'},
		],
		sortInfo:{field: 'perId', direction: 'ASC'}
	});
	PerfilesDataStore.load();*/

        var tipoUsuario = [['Admin'],	['Monitor'],['Egresado'] ];
        
        PerfilesDataStore = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoUsuario
    	});
    
    	var tipoIdentificacion = [
    	['Cedula de Ciudadania'],
    	['Tarjeta de Identidad'],
    	['Cedula de Extrangeria']
    	];
    
    	var comboTipoIdentificacion = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoIdentificacion
    	});


        var gridListado = new Ext.grid.GridPanel({
		//	xtype: 'grid',
			id:	'listaUsuarios',
			bbar: new Ext.PagingToolbar({
				pageSize: 10,
				store: UsuariosDataStore,
				displayInfo: true,
				displayMsg: 'Usuarios {0} - {1} de {2}',
				emptyMsg: "No hay Usuarios"
			}),
			ds: UsuariosDataStore,
			cm: UsuariosColumnModel,
			//width:400,
			sm: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {
  				rowselect: function(sm, row, rec) 
                {
    				myMask = new Ext.LoadMask(formularioUsuario.getEl(), {msg:'Cargando...',removeMask: true});
    				myMask.show();
    				setTimeout('myMask.hide()',500);
    				formularioUsuario.getForm().loadRecord(rec);
    				nombreU = Ext.getCmp('nombre').getValue() +' '+ Ext.getCmp('apellido').getValue();
    				titulo = 'Actualizar Datos de '+nombreU;
    				
    				Ext.getCmp('btGuardar').setText('Actualizar');
    				Ext.getCmp('panelDatos').setTitle(titulo);
    				Ext.getCmp('nombre').setDisabled(false);
    				Ext.getCmp('apellido').setDisabled(false);
    			//	Ext.getCmp('cedula').setDisabled(false);
                    Ext.getCmp('tipo_identificacion').setDisabled(false);
    				Ext.getCmp('email').setDisabled(false);
    
    				
    				//Ext.getCmp('usu_login').setValue(rec.get('usuId'));
    				//Ext.getCmp('usu_login').setDisabled(true);
    
    				Ext.getCmp('clave').setDisabled(false);
    				Ext.getCmp('reClave').setDisabled(false);
    				Ext.getCmp('login').setDisabled(false);
    				Ext.getCmp('tipo_usuario').setDisabled(false);
    				Ext.getCmp('btGuardar').setDisabled(false);
    				Ext.getCmp('btLimpiar').setDisabled(false);
    				
    				Ext.getCmp('reClave').setValue(rec.get('clave'));
    				
    				
    			//	Ext.getCmp('radios_estado').setDisabled(false);
    			//	Ext.getCmp('usu_habilitado').setValue(true);
    				
    				if(rec.get('estado')==1)
    				{
    					Ext.getCmp('usu_habilitado').setValue(true);
    					Ext.getCmp('usu_deshabilitado').setValue(false);
    				}
    				else 
    				{
    					Ext.getCmp('usu_habilitado').setValue(false);
    					Ext.getCmp('usu_deshabilitado').setValue(true);
    				} 
    				if(rec.get('sexo')=='m')
    				{
    					Ext.getCmp('sexo_m_registro').setValue(true);
    					Ext.getCmp('sexo_f_registro').setValue(false);
    				}
    				else 
    				{
    					Ext.getCmp('sexo_m_registro').setValue(false);
    					Ext.getCmp('sexo_f_registro').setValue(true);
    				} 
                    
                    //	Ext.getCmp('sexo').setValue(rec.get('sexo'));    				
  			    }
			}
		}),
		//bodyStyle:'padding:10px',
		height: 400,
		title:'Lista De Usuarios',
		frame:true,
		border: true,
		        listeners: {
		        	render: function(g) {
		        		g.getSelectionModel().selectRow(0);
		        	},
		        	delay: 10
		        },
			tbar: [{
				text: 'Nuevo',
				tooltip: 'Agregar un usuario',
				handler: function()
				{
					myMask = new Ext.LoadMask(formularioUsuario.getEl(), {msg:'Cargando...',removeMask: true});
					myMask.show();
					setTimeout('myMask.hide()',500);
					titulo = 'Nuevo Usuario';
					Ext.getCmp('panelDatos').setTitle(titulo);
					formularioUsuario.getForm().reset();
					

                    Ext.getCmp('nombre').setDisabled(false);
                    Ext.getCmp('apellido').setDisabled(false);
                    Ext.getCmp('cedula').setDisabled(false);
                    Ext.getCmp('tipo_identificacion').setDisabled(false);
                    Ext.getCmp('email').setDisabled(false);
                    
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                    Ext.getCmp('clave').setDisabled(false);
                    Ext.getCmp('reClave').setDisabled(false);
                    Ext.getCmp('login').setDisabled(false);
                    Ext.getCmp('tipo_usuario').setDisabled(false);
                    Ext.getCmp('btGuardar').setDisabled(false);
                	Ext.getCmp('btLimpiar').setDisabled(false);
                    
                    Ext.getCmp('btGuardar').setText('Crear');
				

				},
				iconCls:'add_user',
				xtype: 'tbbutton'
			},{
				text: 'Borrar',
				tooltip: 'Borra un usuario',
				handler: confirDelete1,
				iconCls:'del_user'
			},{
				text: '',
				tooltip: 'Usuarios habilitados',
				iconCls:'user_on',
				handler: function()
				{
                    UsuariosDataStore.baseParams.estado = '1';
    				UsuariosDataStore.load({params: {start: 0, limit: 10}});
				}
				
			},{
				text: '',
				tooltip: 'Usuarios deshabilitados',
				handler: function()
				{
                    UsuariosDataStore.baseParams.estado = '0';
    				UsuariosDataStore.load({params: {start: 0, limit: 10}});
				},
				iconCls:'user_off'
			},{
				text: '',
				tooltip: 'Todos los Usuarios ',
				handler: function()
				{
    				UsuariosDataStore.baseParams.estado = null;
                    UsuariosDataStore.load({params: {start: 0, limit: 10}});
				},
				iconCls:'user_all'
			}],
			plugins:[new Ext.ux.grid.Search({
				mode:          'local',
				position:      top,
				searchText:    'Filtrar',
				iconCls:  'buscar',
				selectAllText: 'Seleccionar todos',
				searchTipText: 'Escriba el texto que desea buscar y presione la tecla enter',
				width:         100
			})]
		});
		
        var panelInfoPersonal ={
                x:10,
                y:5,
                xtype: 'panel',
                labelWidth: 80,
                title:'Informacion Personal',
                defaultType: 'textfield',
                frame:true,
                border: true,
                collapsible: true,
                bodyStyle:'padding:10px',
    			height:300,
			    width:260,
    			layout: 'form',
    			items: [{
					xtype:  'panel',
					height: 30,
					border: false,
					items:
					[{
						xtype: 'label',
						text:  'los campos con (*) no son obligatorios',
						style: 'font-size:8.5pt; color:#484848;font-weight: bold;',
					
					}]
					},{
						fieldLabel: 'Nombre',
						name: 'nombre',
						id: 'nombre',
						width: 140,
						disabled:true,
						allowBlank:false,
						blankText: 'El Nombre es obligatorio'
					},{
						fieldLabel: 'Apellido',
						name: 'apellido',
						id: 'apellido',
						allowBlank:false,
						disabled:true,
						width: 140,
						blankText: 'El Apellido es obligatorio'
					},{
                        xtype:      'radiogroup',
                        fieldLabel: 'Sexo',
                        //style:'background:#f3f3f3;',
                       
                        items: 
                        [{
                          boxLabel:   'F',
                          name:       'sexo',
                          id:         'sexo_f_registro',
                          inputValue: 'f'
                          
                        },
                        {
                          boxLabel:   'M',
                          name:       'sexo',
                          id:         'sexo_m_registro',
                          inputValue: 'm',
                        }]
                  
                      },{
 						fieldLabel: 'Identificacion',
						name: 'cedula',
						vtype:'cedula',
						id: 'cedula',
						allowBlank:false,
						disabled:true,
						width: 140,
						blankText: 'La Cedula es obligatoria'
					},{
						xtype:      'combo',
						fieldLabel: 'Tipo ident.',
						id:         'tipo_identificacion',
						name:'tipo_identificacion',
						store:comboTipoIdentificacion,
						mode:'local',
						displayField: 'opciones',
						triggerAction: 'all',
						emptyText: 'Seleccione ...',
						disabled:true,
						selectOnFocus: true,
						width: 140

					},{
                        fieldLabel: 'Tel&eacute;fono',
                        name: 'telefono',
                        id: 'telefono',
                        allowBlank:false,
                        width: 140,
                        blankText: 'El Telefono es obligatorio'
                      },{
                        fieldLabel: 'Celular(*)',
                        name: 'celular',
                        id: 'celular',
                        allowBlank:true,
                        width: 140,
                        blankText: 'El Telefono es obligatorio'
                      },{
						fieldLabel: 'Email(*)',
						name: 'email',
						id: 'email',
						vtype:  'email',
						allowBlank:true,
						disabled:true,
						width: 140,
						minLength : 5,
						maxLength : 30,
						blankText: 'El Email no es obligatorio'
					 }
				]};	
                	
        var detalleRegistro ={
			
                x:270,
                y:5,
                xtype: 'panel',
                labelWidth: 90,
                frame:true,
                border: true,
                collapsible: true,
                layout:'form',
    			title:'Detalles del Sistema',
				defaultType: 'textfield',
			    height:300,
				width:260,
			//	bodyStyle: Ext.isIE ? 'padding:0 0 35px 15px;' : 'padding:37px 15px;',
			// bodyStyle:'background:#F2F2F2;padding:10px;',
			    bodyStyle:'padding:10px',
				
				style: {
					"margin-left": "10px", // when you add custom margin in IE 6...
					"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
				},
				items: [{
					xtype:  'panel',
					height: 30,
					border: false,
					items:
					[{
						xtype: 'label',
						text:  'Todos los campos son obligatorios',
						style: 'font-size:8.5pt; color:#484848;font-weight: bold;',
					}]
					},{
 						fieldLabel: 'Usuario',
						name: 'login',
						id: 'login',
						allowBlank:false,
						disabled:true,
						width: 140,
						blankText: 'Login es Obligatorio'
					},{
						fieldLabel: 'Contrase&ntilde;a',
						name: 'clave',
						id: 'clave',
						inputType:'password',
						allowBlank:false,
						disabled:true,
						width: 140,
						blankText: 'La Contrase�a es obligatoria'
					},{
						fieldLabel: 'ReContrase&ntilde;a',
						name: 'reClave',
						id: 'reClave',
						initialPassField: 'usu_clave',
						inputType:'password',
						allowBlank:false,
						disabled:true,
						width: 140,
						blankText: 'La ReContrase�a es obligatoria'
					},{
						xtype: 'combo',
						store: PerfilesDataStore,
						name: 'tipo_usuario',
						id: 'tipo_usuario',
						mode:'local',
						displayField: 'opciones',
						triggerAction: 'all',
						emptyText: 'Seleccione ...',
						disabled:true,
						selectOnFocus: true,
						width: 140,
						fieldLabel: 'Tipo Usuario',
					},{
                    xtype: 'panel',
                    height:10
                    
                    },{
						xtype: 'panel',
						layout: 'form',
					
						
						items:
						[{
							xtype:      'radiogroup',
							fieldLabel: 'Estado',
						
							name: 'radios_estado',
							id:    'radios_estado',
							items: 
							[{
							column: '.5',
								items:
								[{
									boxLabel:   'Habilitado',
									name:       'usuEstado',
									id:         'usu_habilitado'
								
                                    ,inputValue: 't'
								},
								{
									boxLabel:   'Deshabilitado',
									name:       'usuEstado',
								
                                    id:         'usu_deshabilitado',
									inputValue: 'f'
								}]
							}]
						}]
					}]
			};
            
            		
	var formularioUsuario = new Ext.FormPanel({
        	id: 'formUsuario',
        	url: 'usuarioPersona/cargar',
        	frame: true,
        	labelAlign: 'left',
        	title: 'Gestion De Usuarios',
        	bodyStyle:'padding:5px',
        	width: 1050,
        	height: 500,
        	autoScroll:true,
        	layout: 'absolute',
        	items: [{
        		x:5,
        		y:0,
        		layout: 'fit',
        		autoScroll:true,
        		items: gridListado
                },{
        	//	columnWidth: 0.57,
        		x:460,
        		y:0,
        		xtype:  'panel',
        		id:'panelDatos',
        		title:'Actualizar Datos del Usuario',
        		defaultType: 'textfield',
        		height: 400,
        		
        		frame:true,
        		layout: 'absolute',
        	//	bodyStyle: Ext.isIE ? 'padding:0 0 20px 15px;' : 'padding:10px 15px;',
        	    //bodyStyle:'padding:10px',
                width: 570,
        
        	/*	style: {
        			"margin-left": "10px", // when you add custom margin in IE 6...
        			"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        		}, */
        
                    items:[panelInfoPersonal,
                            detalleRegistro],
        			buttons:[
                			{
                			text:'Actualizar',
                			formBind: true,
                			align:'center',
                			id: 'btGuardar',
                			iconCls:'salvar',
                			disabled:true,
                			handler:function(formulario,accion)
                			{
                			var verificacion = verificarCampos();
                			if(verificacion  ==  'almacenar')
                			{
                			/*	var contrasenaEncrypt = '';
                				if(Ext.getCmp('usu_clave').getValue() != ''){
                				contrasenaEncrypt = hex_md5(Ext.getCmp('usu_clave').getValue());
                				}*/
                				var estado = '0';
                				if(Ext.getCmp('usu_habilitado').checked){
                				    estado = '1';
                				}
                				
                			/*	var sexo = 'm';
                				if(Ext.getCmp('sexo_f_registro').checked){
                				    sexo = 'f';
                				}
                				*/
                				if (Ext.getCmp('btGuardar').getText() == 'Actualizar')
                				{
                					task = 'UPDATEUSU';
                				//	Ext.getCmp('usu_id').setDisabled(false);
                				}
                				else
                				{
                					task = 'CREATEUSU';
                				}
                				Ext.getCmp('cedula').setDisabled(false);
                				
                				formularioUsuario.getForm().submit({
                				method: 'POST',
                				params: {
                				task: task,
                				estado: estado,
                			//	sexo: sexo,
                				},
                				waitTitle: 'Enviando',
                				waitMsg: 'Enviando datos...',
                				success: function(response, action)
                				{
                		obj = Ext.util.JSON.decode(action.response.responseText);
                					Ext.Msg.show
                					({
                						title:'Alerta!',
                						msg: obj.mensaje,
                						buttons: Ext.Msg.OK,
                						animEl: 'elId',
                						icon: Ext.MessageBox.INFO
                					});
                					UsuariosDataStore.reload();
                				    Ext.getCmp('cedula').setDisabled(true);
                				},
                				failure: function(form, action, response)
                				{
                				if(action.failureType == 'server'){
                					obj = Ext.util.JSON.decode(action.response.responseText); 
                					Ext.Msg.show
                					({
                						title:'Error',
                						msg: obj.errors.reason,
                						buttons: Ext.Msg.OK,
                						animEl: 'elId',
                						icon: Ext.MessageBox.ERROR
                					});
                				}
                				else 
						{ 
							Ext.Msg.show
                					({
                						title:'Error',
                						msg: 'Error de ejecucion del script!',
                						buttons: Ext.Msg.OK,
                						animEl: 'elId',
                						icon: Ext.MessageBox.ERROR
                					});
                				}
                			//	Ext.getCmp('usu_contrasena').setDisabled(false);
                			//	Ext.getCmp('usu_recontrasena').setDisabled(false);
                				}
                				});
                			}
                			}
                			},{
                			text:'Limpiar',
                			align:'center',
                			formBind: true,
                			id: 'btLimpiar',
                			iconCls:'limpiar',
                			disabled:true,
                			handler:function()
                				{ 
                				  var cedula =Ext.getCmp('cedula').getValue();
                					formularioUsuario.getForm().reset();
                					Ext.getCmp('cedula').setValue(cedula);
                				}
                			}
        			]
        
        		}],
        		renderTo: 'formulario'
	});
	
var verificarCampos=function ()

{

		salida =  'almacenar';
		

		if(Ext.getCmp('nombre').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un nombre', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('nombre').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('nombre').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un apellido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('apellido').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('sexo_m_registro').checked == false && Ext.getCmp('sexo_f_registro').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione un Sexo', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir el n&uacute;mero de c&eacute;dula', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue().length < 8 || Ext.getCmp('cedula').getValue().length > 10 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe estar en un rango de 8 a 10 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('cedula').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe ser n�merica', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('tipo_identificacion').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de identificaci&oacute;n', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('tipo_identificacion').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El tipo no debe tener caracteres num&eacute;ricos ', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('telefono').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un t&eacute;lefono', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('telefono').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El t&eacute;lefono debe ser num&eacute;rico', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un usuario', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue().length < 5 && Ext.getCmp('login').getValue().length > 20 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El usuario debe estar en un rango de 5 a 20 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() != Ext.getCmp('reClave').getValue()) 
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La contrase&ntilde;a y su confirmaci&oacute;n, no coinciden', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir una contrase&ntilde;a', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if((Ext.getCmp('tipo_usuario').getValue() != 'Egresado')&&(Ext.getCmp('tipo_usuario').getValue() != 'Monitor')&&(Ext.getCmp('tipo_usuario').getValue() != 'Admin'))
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de usuario v&aacute;lido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('usu_habilitado').checked == false && Ext.getCmp('usu_deshabilitado').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione el estado de la cuenta', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
					
	
		
	return salida;
	

}

<!-- 	 ESTE ES EL METODO QUE ME PERMITE MOSTRAR EL MENSAJE DE CONFIRMACION PARA EL BORRADO DE LOS USUARIOS EN EL GRIP -->
	function confirDelete1()
	{
		if(UsuariosListingEditorGrid.selModel.getCount() != 1)
		{
		//	Ext.MessageBox.confiirm('Confirmacion','Desea borrar este Usuario?', borrarUsuario);
			
			
			
Ext.Msg.show({
   title:'Save Changes?',
   msg: 'Desea borrar este Usuario?',
   buttons: Ext.Msg.YESNO,
   fn: borrarUsuario,
   animEl: 'elId',
   icon: Ext.MessageBox.QUESTION
});

			
		} else if(UsuariosListingEditorGrid.selModel.getCount() > 1){
			Ext.MessageBox.confirm('Confirmacion','Borrar estos Usuarios?', borrarUsuario);
		} else {
			Ext.MessageBox.alert('Advertencia','Usted no puede borrar un elemento que no ha sido seleccionado');
		}
	
	}
	

<!-- 	 ESTE ES EL METODO-->
	function borrarUsuario(btn){
		if(btn=='yes'){
			var selections = UsuariosListingEditorGrid.selModel.getSelections();
			var prez = [];
			fila	=	Ext.getCmp('listaUsuarios').selModel.getSelected();
			identificador	=	fila.get('cedula');
			
					/*			Ext.Msg.show
					({
						title:'Error',
						msg: 'cc:'+identificador,
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});*/
					
			prez.push(identificador);
			var encoded_array = Ext.encode(prez);
			Ext.Ajax.request({  
				waitMsg: 'Por Favor Espere...',
   				url: 'usuarioPersona/cargar', 
				params: { 
					task: "DELETEUSU", 
					ids:  encoded_array
				}, 
				success: function(response){
          obj = Ext.util.JSON.decode(response.responseText);
					if (obj.success)
					{
						Ext.Msg.show
						({
							title:'Alerta!',
							msg: obj.mensaje,
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
						UsuariosDataStore.reload();
					}
					else if (obj.success == false)
					{
						Ext.MessageBox.show
						({
							title: 'Advertencia',
							msg: obj.errors.reason,
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.WARNING
						});
					}
				},
				failure: function(response){
					var result=response.responseText;
					Ext.Msg.show
					({
						title:'Error',
						msg: 'No se pudo conectar con la base de datos',
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});
				}
			});
		}
	}
  } 
  }
}();
