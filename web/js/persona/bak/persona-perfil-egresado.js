



var personaEgresado = function () 
{

return { 
		init: function () {
		
    	var UsuariosDataStore;
    	var UsuariosColumnModel;
    	var UsuariosListingEditorGrid;
    	var UsuariosListingWindow;
    	var usuariosHabilitados=true;
    	var PerfilesDataStore;
 
        Ext.form.VTypes.cedulaVal  = /^[0-9]+/;
		Ext.form.VTypes.cedulaMask = /[0-9]/;
		Ext.form.VTypes.cedulaText = 'C�dula Invalida.';
		Ext.form.VTypes.cedula 	= function(v){
			return Ext.form.VTypes.cedulaVal.test(v);
		};
             
	

    	UsuariosDataStore = new Ext.data.JsonStore({
    		id: 'UsuariosDataStore',
    		url:'perfilEgresado/cargar',
    		root: 'results',
    		baseParams:{task:'LISTING'},
    		totalProperty: 'total',
    		fields:[
                
                {name: 'tipo_usuario', type: 'string'},
    		    {name: 'id', type: 'int'},
                {name: 'estado', type: 'int'},
                {name: 'login', type: 'string'},
    			{name: 'clave', type: 'string'},
                
                // DATSO PERSONA
    			{name: 'cedula', type: 'string'},
    			{name: 'tipo_cedula', type: 'string'},
    			{name: 'nombre', type: 'string'},
    			{name: 'apellido', type: 'string'},
    			{name: 'sexo', type: 'string'},
    			{name: 'telefono', type: 'string'},
                {name: 'celular', type: 'string'},
    			{name: 'email', type: 'string'},
                {name: 'direccion', type: 'string'},
                {name: 'ciudad', type: 'string'},
                {name: 'fecha_nacimiento', type: 'string'},
                {name: 'departamento', type: 'string'},
  
    			
    			
    			
    			// DATOS PERFIL
                {name: 'nivel_academico', type: 'string'},
                {name: 'area_estudio', type: 'string'},
                {name: 'estado_estudio', type: 'string'},
                {name: 'estado_perfil', type: 'string'},
                {name: 'idiomas', type: 'string'},
                {name: 'becas', type: 'string'},
                {name: 'resumen', type: 'string'}
    											
    		],
    		sortInfo:{field: 'cedula', direction: 'ASC'}
    	});
    	UsuariosDataStore.load({params: {start: 0, limit: 10}});
    
      
    	UsuariosColumnModel = new Ext.grid.ColumnModel(
    		[{
    			header: 'cedula',
    			dataIndex: 'cedula',
                sortable:true,
    			width: 105
    		},{
    			header: 'Nombres',
    			dataIndex: 'nombre',
                sortable:true,
    			width: 105
    		},{
    			header: 'Apellidos',
    			dataIndex: 'apellido',
                sortable:true,
    			width: 105
    		},{
    			header: 'Estado Perfil',
    			dataIndex: 'estado_perfil',
                sortable:true,
    			width: 100
    		}
    	]);
    	
    	UsuariosColumnModel.defaultSortable= true;
    
    	UsuariosListingEditorGrid =  new Ext.grid.EditorGridPanel({
    		id: 'UsuariosListingEditorGrid',
    		store: UsuariosDataStore,
    		cm: UsuariosColumnModel,
    		enableColLock:true,
    		clicksToEdit:1,
    	
    		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
    	});
    
    	UsuariosListingWindow = new Ext.Panel({
    		id: 'UsuariosListingWindow',
    		title: 'Informacion de Usuarios',
    		closable:true,
    		width:100,
    		height:390,
    		plain:true,
    		layout: 'fit',
    		frame: true,
    		items: UsuariosListingEditorGrid
    	});



/*	PerfilesDataStore = new Ext.data.JsonStore({
		id: 'UsuariosDataStore',
		url:'usuarios/cargar',
		root: 'results',
		baseParams:{task:'LISTINGPERFIL'},
		totalProperty: 'total',
		fields:[
			{name: 'perId', type: 'string'},
			{name: 'perNombre', type: 'string'},
		],
		sortInfo:{field: 'perId', direction: 'ASC'}
	});
	PerfilesDataStore.load();*/

        var tipoUsuario = [['Admin'],	['Monitor'],['Egresado'] ];
        
        PerfilesDataStore = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoUsuario
    	});
    
    	var tipoIdentificacion = [
    	['Cedula de Ciudadania'],
    	['Tarjeta de Identidad'],
    	['Cedula de Extrangeria']
    	];
    	
    	var nivelAcademico = [
    	['Doctorado'],
    	['Maestria'],
    	['Postgrado'],
    	['Pregrado'],
     	['Tecnologo']
    	];
  	

        
    	var estadoEstudios = [
    	['Terminado'],
    	['Estudiando'],
    	['Suspendido']
    	];        
            
    	var estadoPerfil = [
    	['Empleado'],
    	['Desempleado']
    	];
                    
    	var comboTipoIdentificacion = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoIdentificacion
    	});
    	
    	var comboNivelAcademico = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : nivelAcademico
    	});

    	var comboEstadoEstudios = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : estadoEstudios
    	});
        
    	var comboEstadoPerfil = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : estadoPerfil
    	});        
    	
    	
        var gridListadoPerfil = new Ext.grid.GridPanel({
		//	xtype: 'grid',
			id:	'listaPerfilEgresado',
			bbar: new Ext.PagingToolbar({
				pageSize: 10,
				store: UsuariosDataStore,
				displayInfo: true,
				displayMsg: 'Usuarios {0} - {1} de {2}',
				emptyMsg: "No hay Usuarios"
			}),
			ds: UsuariosDataStore,
			cm: UsuariosColumnModel,
			//width:400,
			sm: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {
  				rowselect: function(sm, row, rec) 
                {
                    myMask = new Ext.LoadMask(formularioPerfil.getEl(), {msg:'Cargando...',removeMask: true});
                    myMask.show();
                    setTimeout('myMask.hide()',500);
                    //	Ext.getCmp('egr_cedula').setValue('Hola');
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg:	Ext.getCmp('perfilPanelDatos').getId(), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    formularioPerfil.getForm().loadRecord(rec);
                    
                    
                    
                    nombreU = Ext.getCmp('egr_nombre').getValue() +' '+ Ext.getCmp('egr_apellido').getValue();
                    
                    
                    titulo = 'Actualizar Datos de '+nombreU;
                    
                    Ext.getCmp('perfilPanelDatos').setTitle(titulo);
    				
    				Ext.getCmp('egr_btGuardar').setText('Actualizar');
    				
    			/*	Ext.getCmp('egr_nombre').setDisabled(false);
    				Ext.getCmp('egr_apellido').setDisabled(false);
    				Ext.getCmp('egr_email').setDisabled(false);
    				
    				Ext.getCmp('egr_email').setDisabled(false);*/
    				
    				
    						
    					
    				
    				
    				if(rec.get('sexo')=='M')
    				{
    					Ext.getCmp('egr_sexo_m_registro').setValue(true);
    					Ext.getCmp('egr_sexo_f_registro').setValue(false);
    				}
    				else 
    				{
    					Ext.getCmp('egr_sexo_m_registro').setValue(false);
    					Ext.getCmp('egr_sexo_f_registro').setValue(true);
    				}
    				
    
    				  //Ext.getCmp('empleado').setValue(true);
    				
    					Ext.getCmp('egr_btGuardar').setDisabled(false);
    					Ext.getCmp('egr_btLimpiar').setDisabled(false);
                    
                        				
                    //	Ext.getCmp('cedula').setDisabled(false);
                    //   Ext.getCmp('tipo_identificacion').setDisabled(false);
                    //
                    // Ext.getCmp('egr_sexo_m_registro').setValue(true);
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                    //	Ext.getCmp('clave').setDisabled(false);
                    //	Ext.getCmp('reClave').setDisabled(false);
                    //	Ext.getCmp('login').setDisabled(false);
                    //	Ext.getCmp('tipo_usuario').setDisabled(false);
                    
                    
                    
                    //	Ext.getCmp('reClave').setValue(rec.get('clave'));
                    
                    
                    //	Ext.getCmp('radios_estado').setDisabled(false);
                    //	Ext.getCmp('usu_habilitado').setValue(true);
                    
                    //	Ext.getCmp('nombre').setValue('sexo:'+rec.get('sexo'));
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg: rec.get('sexo'), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    
                    
                    
                    //	Ext.getCmp('sexo').setValue(rec.get('sexo'));    				
        }
			}
		}),
		//bodyStyle:'padding:10px',
		height: 400,
		title:'Lista de Egresados',
		frame:true,
		border: true,
		        listeners: {
		        	render: function(g) {
		        		g.getSelectionModel().selectRow(0);
		        	},
		        	delay: 10
		        },
			tbar: [
            
            /*
            {
				text: 'Nuevo',
				tooltip: 'Agregar un usuario',
				handler: function()
				{
					myMask = new Ext.LoadMask(formularioPerfil.getEl(), {msg:'Cargando...',removeMask: true});
					myMask.show();
					setTimeout('myMask.hide()',500);
					titulo = 'Nuevo Usuario';
					Ext.getCmp('perfilPanelDatos').setTitle(titulo);
					formularioPerfil.getForm().reset();
					

                    Ext.getCmp('egr_nombre').setDisabled(false);
                    Ext.getCmp('egr_apellido').setDisabled(false);
                    Ext.getCmp('egr_cedula').setDisabled(false);
                    Ext.getCmp('egr_tipo_identificacion').setDisabled(false);
                    Ext.getCmp('egr_email').setDisabled(false);
                    
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                   // Ext.getCmp('clave').setDisabled(false);
                  //  Ext.getCmp('reClave').setDisabled(false);
                  //  Ext.getCmp('login').setDisabled(false);
                  //  Ext.getCmp('tipo_usuario').setDisabled(false);
                    Ext.getCmp('egr_btGuardar').setDisabled(false);
                	Ext.getCmp('egr_btLimpiar').setDisabled(false);
                    
                    Ext.getCmp('egr_btGuardar').setText('Crear');
				

				},
				iconCls:'add_user',
				xtype: 'tbbutton'
			},{
				text: 'Borrar',
				tooltip: 'Borra un usuario',
				handler: confirDelete1,
				iconCls:'del_user'
			},
            
            */
            
            {
				text: '',
				tooltip: 'Usuarios Empleados',
				iconCls:'user_empleado',
				handler: function()
				{
                    UsuariosDataStore.baseParams.estado_perfil = 'Empleado';
    				UsuariosDataStore.load({params: {start: 0, limit: 10}});
				}
				
			},{
				text: '',
				tooltip: 'Usuarios Desempleados',
				handler: function()
				{
                    UsuariosDataStore.baseParams.estado_perfil = 'Desempleado';
    				UsuariosDataStore.load({params: {start: 0, limit: 10}});
				},
				iconCls:'user_desempleado'
			},{
				text: '',
				tooltip: 'Todos los Usuarios ',
				handler: function()
				{
    				UsuariosDataStore.baseParams.estado_perfil = null;
                    UsuariosDataStore.load({params: {start: 0, limit: 10}});
				},
				iconCls:'user_all'
			}],
			plugins:[new Ext.ux.grid.Search({
				mode:          'local',
				position:      top,
				searchText:    'Filtrar',
				iconCls:  'buscar',
				selectAllText: 'Seleccionar todos',
				searchTipText: 'Escriba el texto que desea buscar y presione la tecla enter',
				width:         100
			})]
		});
		
    
            
  
var panelUpload = {
       // renderTo: 'fi-form',
       layout:'form',
        fileUpload: true,
        width: 465,
        frame: true,
       border:false,
        title: 'Subir Hoja de Vida',
       

       // autoHeight: true,
        bodyStyle: 'padding: 10px 10px 0 10px;',
        labelWidth: 50,
        defaults: {
            anchor: '95%',
            allowBlank: false,
            msgTarget: 'side'
        },
        items: [{
            xtype: 'fileuploadfield',
            id: 'form-file',
            emptyText: 'Select an image',
            fieldLabel: 'Archivo',
            name: 'photo-path',
            buttonText: '',
            buttonCfg: {
                iconCls: 'upload-icon'
            }
        }],
        buttons: [{
            text: 'Save',
            handler: function(){
                if(fp.getForm().isValid()){
	                fp.getForm().submit({
	                    url: 'file-upload.php',
	                    waitMsg: 'Uploading your photo...',
	                    success: function(fp, o){
	                        msg('Success', 'Processed file "'+o.result.file+'" on the server');
	                    }
	                });
                }
            }
        },{
            text: 'Reset',
            handler: function(){
                fp.getForm().reset();
            }
        }]
    };
    
    
      var tab2 = {
       xtype: 'panel',
        labelAlign: 'top',
      //  title: 'Inner Tabs',
        bodyStyle:'padding:5px',
        width: 500,
        height:600,
        x:5,
        y:0,
        layout:'absolute',
        items: [{
            layout:'column',
                    x:5,
        y:5,
         width: 500,
            border:false,
            items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'Nombre',
                    name: 'nombre',
                    id: 'egr_nombre',
                    anchor:'95%'
                }, {
                    xtype:'textfield',
                    fieldLabel: 'Apellido',
                    name: 'apellido',
                    id: 'egr_apellido',
                    anchor:'95%'
                }]
            },{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items:[{
                        xtype:'textfield',
 						fieldLabel: 'Identificacion',
						name: 'cedula',
						vtype:'cedula',
						id: 'egr_cedula',
						allowBlank:false,
						disabled:true,
					    anchor:'95%'
				
					}, {
						xtype:      'combo',
						fieldLabel: 'Tipo ident.',
						id:         'egr_tipo_cedula',
						name:'tipo_cedula',
						store:comboTipoIdentificacion,
						mode:'local',
						displayField: 'opciones',
						triggerAction: 'all',
						emptyText: 'Seleccione ...',
					//	disabled:true,
						selectOnFocus: true,
					    anchor:'95%'

					}]
      
            }]}
        ,{
            xtype:'tabpanel',
            plain:true,
            activeTab: 0,
            height:230,
            x:5,
            y:100,
            defaults:{bodyStyle:'padding:10px'},
            items:[{
                title:'Detalles Personales',
                layout:'form',
                defaults: {width: 230},
              //  defaultType: 'textfield',

                items: [{
                layout:'column',
                x:5,
                y:5,
                width: 500,
                border:false,
                items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    
                        fieldLabel: 'Tel&eacute;fono',
                        name: 'telefono',
                        xtype:'textfield',
                        id: 'egr_telefono',
                        allowBlank:false,
                        width: 200,
                        blankText: 'El Tel�fono es obligatorio'
                      },{
                        fieldLabel: 'Celular',
                        xtype:'textfield',
                        name: 'celular',
                        id: 'egr_celular',
                        allowBlank:true,
                        width: 200,
                        blankText: 'El Tel�fono celular es obligatorio'
                      },{
                        fieldLabel: 'Direccion',
                        xtype:'textfield',
                        name: 'direccion',
                        id: 'egr_direccion',
                        width: 200
                        
                      },{
                        xtype:      'radiogroup',
                        fieldLabel: 'Sexo',
                        //style:'background:#f3f3f3;',
                       
                        items: 
                        [{
                          boxLabel:   'F',
                          name:       'egr_sexo',
                          id:         'egr_sexo_f_registro',
                          inputValue: 'F'
                          
                        },
                        {
                          boxLabel:   'M',
                          name:       'egr_sexo',
                          id:         'egr_sexo_m_registro',
                          inputValue: 'M',
                        }]
                  
                      }
                      ]
                      },
                      {
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
       
                      
						fieldLabel: 'E-mail',
                        xtype:'textfield',
						name: 'email',
						id: 'egr_email',
						vtype:  'email',
						allowBlank:true,
						width: 200,
						minLength : 5,
						maxLength : 30,
						blankText: 'El Email no es obligatorio'
					 },{
       
                      
						fieldLabel: 'Fecha de Nacimiento',
                        xtype:'datefield',
						name: 'fecha_nacimiento',
						id: 'egr_fecha',
                        format : "Y-m-d",
                        value:'A�o-Mes-D�a',

					
						allowBlank:true,
						width: 200,
				
					 },{
                        fieldLabel: 'Ciudad',
                        xtype:'textfield',
                        name: 'ciudad',
                        id: 'egr_ciudad',
                        width: 200
                        
                      },{
                        fieldLabel: 'Departamento',
                        xtype:'textfield',
                        name: 'departamento',
                        id: 'egr_depto',
                        width: 200
                        
                      }
                     
                     
                     
                     
                     
                     
                     
                     ]}
                      ]
            }
            ]
            },
            {
                
                title:'Detalles Perfil',
                layout:'form',
               
                defaults: {width: 480},
            //  autoWidth:true,
              autoHeight:true,
              //  defaultType: 'textfield',

                items: [{
                layout:'column',
                border:false,
                items:[{
                    columnWidth:.5,
                    layout: 'form',
                    border:false,
                    items: [
                    
                    
                            {
            				xtype: 'combo',
            				store: comboNivelAcademico,
            				name: 'nivel_academico',
            				id: 'egr_nivel_academico',
            				mode:'local',
            				displayField: 'opciones',
            				triggerAction: 'all',
            				emptyText: 'Seleccione ...',
            		
            				selectOnFocus: true,
            					width: 200,
            				fieldLabel: 'Nivel Acad&eacute;mico',
            			},{
            				fieldLabel: 'Area de Estudio',
                            xtype:'textfield',
            				name: 'area_estudio',
            				id: 'egr_area_estudio',
            				  	width: 200,
            				blankText: 'El &aacute;rea es obligatoria'
            			}
                        ]
                        },
                        {
                        columnWidth:.5,
                        layout: 'form',
                        border:false,
                        items:[{
            
                   
          
                				xtype: 'combo',
                				store: comboEstadoEstudios,
                				name: 'estado_estudio',
                				id: 'egr_estado_estudio',
                				mode:'local',
                				displayField: 'opciones',
                				triggerAction: 'all',
                				emptyText: 'Seleccione ...',
                		
                				selectOnFocus: true,
                				width: 200,
                				fieldLabel: 'Estado Estudios',
                			},
                            
                            {
                				xtype: 'combo',
                				store: comboEstadoPerfil,
                				name: 'estado_perfil',
                				id: 'egr_estado_perfil',
                				mode:'local',
                				displayField: 'opciones',
                				triggerAction: 'all',
                				emptyText: 'Seleccione ...',
                		
                				selectOnFocus: true,
                				 	width: 200,
                				fieldLabel: 'Estado Perfil',
                			}
                            ]
                            }
                            ]
                            }
            
            


            /*
            {
		
					xtype:      'radiogroup',
					fieldLabel: 'Estado',
					id:'radio_estado_empleo',
                   
			
					items: 
					[{
						boxLabel:   'Desempleado',
						name: "pefEstado",
						id:         'desempleado',

						inputValue: '2'
						},
						{
						boxLabel:   'Empleado',

						name: "pefEstado",
						id:         'empleado',
						inputValue: '1'
						}
					]
			
			} */
                
    		]
            }    ,{
                title:'Info Adicional',
                layout:'form',
              
                defaults: {width: 230},
               // defaultType: 'textfield',

                items: [{
                    
                layout:'column',
                x:5,
                y:5,
                width: 500,
                border:false,
                items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    
						xtype: 'textarea',
						name: 'becas',
						id: 'egr_becas',
   					    selectOnFocus: true,
						width: 220,
						fieldLabel: 'Becas',
					}
                    ]
                    }
                    
                    
                    
                    
                ,{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
						xtype: 'textarea',
						name: 'idiomas',
						id: 'egr_idiomas',
   					    selectOnFocus: true,
						width: 220,
						fieldLabel: 'Idiomas',
					}]}
                    ]
                    }
                    ]
                    },
                    
                 /*   {
              //  cls:'x-plain',
                title:'Hoja de Vida',
               
                items: panelUpload
            }, */
            
            
            {
                    title:'Resumen',
                    layout:'form',
                  
                    defaults: {width: 230},
                   // defaultType: 'textfield',
    
                    items: [{
    						xtype: 'panel',
    						name: 'info-resumen',
    						id: 'egr_info-resumen',
       					    selectOnFocus: true,
    						width: 200,
    						
    					},{
    						xtype: 'textarea',
    						name: 'resumen',
    						id: 'egr_resumen',
       					    selectOnFocus: true,
    						width: 400,
    						fieldLabel: 'Resumen del Perfil',
					}]}



                    ]
        }

]
    };
 
              		
	var formularioPerfil = new Ext.FormPanel({
        	id: 'formPerfil',
        	url: 'perfilEgresado/cargar',
        	frame: true,
        	labelAlign: 'left',
        	title: 'Gestion De Perfiles',
        	bodyStyle:'padding:5px',
        	width: 1050,
        	height: 500,
        	autoScroll:true,
        	layout: 'absolute',
        	items: [{
        		x:5,
        		y:0,
        		layout: 'fit',
        		autoScroll:true,
        		items: gridListadoPerfil
                },{
        	//	columnWidth: 0.57,
        		x:460,
        		y:0,
        		xtype:  'panel',
        		id:'perfilPanelDatos',
        	
        		title:'Actualizar Datos del Perfil',
        		defaultType: 'textfield',
        		height: 410,
        	  
        		
        		frame:true,
        		layout: 'absolute',
        	//	bodyStyle: Ext.isIE ? 'padding:0 0 20px 15px;' : 'padding:10px 15px;',
        	    //bodyStyle:'padding:10px',
                width: 530,
        
        	/*	style: {
        			"margin-left": "10px", // when you add custom margin in IE 6...
        			"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        		}, */
        
                    //items:[panelInfoPersonal,detalleRegistro],
                    items:[tab2],
                    
        			buttons:[
                			{
                			text:'Actualizar',
                			formBind: true,
                			align:'center',
                			id: 'egr_btGuardar',
                			iconCls:'salvar',
                		//	disabled:true,
                			handler:function(formulario,accion)
                			{
                			var verificacion =  'almacenar';
                            
                            //verificarCampos();
                			if(verificacion  ==  'almacenar')
                			{
                			/*	var contrasenaEncrypt = '';
                				if(Ext.getCmp('usu_clave').getValue() != ''){
                				contrasenaEncrypt = hex_md5(Ext.getCmp('usu_clave').getValue());
                				}*/
                
                			/*	var sexo = 'm';
                				if(Ext.getCmp('sexo_f_registro').checked){
                				    sexo = 'f';
                				}
                				*/
                				if (Ext.getCmp('egr_btGuardar').getText() == 'Actualizar')
                				{
                					task = 'UPDATEUSU';
                				//	Ext.getCmp('usu_id').setDisabled(false);
                				}
                				else
                				{
                					task = 'CREATEUSU';
                				}
                				
                				//Ext.MessageBox.alert('Advertencia','ok');
                				Ext.getCmp('egr_cedula').setDisabled(false);
                				Ext.getCmp('egr_tipo_cedula').setDisabled(false);
                				
                				
                				formularioPerfil.getForm().submit({
                				
                    				method: 'POST',
                    				params: {
                    					task: task
						
                    			
                    			
                    				},
                    				waitTitle: 'Enviando',
                    				waitMsg: 'Enviando datos...',
                    				success: function(response, action)
                    				{
                                        obj = Ext.util.JSON.decode(action.response.responseText);
                                        Ext.Msg.show
                                        ({
                                            title:'Alerta!',
                                            msg: obj.mensaje,
                                            buttons: Ext.Msg.OK,
                                            animEl: 'elId',
                                            icon: Ext.MessageBox.INFO
                                        });
                                        UsuariosDataStore.reload();
                                        Ext.getCmp('egr_cedula').setDisabled(true);
                                       // Ext.getCmp('egr_tipo_cedula').setDisabled(true);
                    				},
                    				failure: function(form, action, response)
                    				{
                        				Ext.getCmp('egr_cedula').setDisabled(true);
                                        Ext.getCmp('egr_tipo_cedula').setDisabled(true);
                                        
                        				if(action.failureType == 'server'){
                        					obj = Ext.util.JSON.decode(action.response.responseText); 
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: 'Error',
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        				}
                        				else if (Ext.getCmp('egr_email').getRawValue()) {
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: 'No se pudo conectar con la base de datos',
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        				} 
                        			//	Ext.getCmp('usu_contrasena').setDisabled(false);
                        			//	Ext.getCmp('usu_recontrasena').setDisabled(false);
                        				}
                				});
                			}
                			}
                			},{
                			text:'Limpiar',
                			align:'center',
                			formBind: true,
                			id: 'egr_btLimpiar',
                			iconCls:'limpiar',
                		//	disabled:true,
                			handler:function()
                				{ 
                				  var cedula =Ext.getCmp('egr_cedula').getValue();
                				  var tipo_cc = Ext.getCmp('egr_tipo_cedula').getValue();
                					formularioPerfil.getForm().reset();
                					Ext.getCmp('egr_cedula').setValue(cedula);
                					Ext.getCmp('egr_tipo_cedula').setValue(tipo_cc);
                					Ext.getCmp('egr_resumen').setValue('');
                					
                				}
                			}
        			]
        
        		}],
        		renderTo: 'persona-egresado'
	});
	
	
	formularioPerfil.form.load({
	            url:'perfilEgresado/cargar',
	        //    method:'GET',
	            params:{task:'LISTING'},
	            waitMsg:'Loading',
	            success:function(form, action) {
	                
	                
	                obj = Ext.util.JSON.decode(action.response.responseText);
	                
	                if(obj.data.sexo=='M')
    				{
    					Ext.getCmp('egr_sexo_m_registro').setValue(true);
    					Ext.getCmp('egr_sexo_f_registro').setValue(false);
    				}
    				else 
    				{
    					Ext.getCmp('egr_sexo_m_registro').setValue(false);
    					Ext.getCmp('egr_sexo_f_registro').setValue(true);
    				}
    				
    			//	Ext.MessageBox.alert('Message', 'Loaded OK');
	            },        	
	            failure:function(form, action) {
	                Ext.MessageBox.alert('Message', 'Problemas en la carga de datos!');
	            }
	        });

	
var verificarCampos=function ()

{

		salida =  'almacenar';
		

		if(Ext.getCmp('nombre').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un nombre', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('nombre').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('nombre').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un apellido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('apellido').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('sexo_m_registro').checked == false && Ext.getCmp('sexo_f_registro').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione un Sexo', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir el n&uacute;mero de c&eacute;dula', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue().length < 8 || Ext.getCmp('cedula').getValue().length > 10 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe estar en un rango de 8 a 10 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('cedula').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe ser n�merica', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('tipo_identificacion').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de identificaci&oacute;n', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('tipo_identificacion').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El tipo no debe tener caracteres num&eacute;ricos ', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('telefono').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un t&eacute;lefono', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('telefono').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El t&eacute;lefono debe ser num&eacute;rico', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un usuario', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue().length < 5 && Ext.getCmp('login').getValue().length > 20 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El usuario debe estar en un rango de 5 a 20 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() != Ext.getCmp('reClave').getValue()) 
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La contrase&ntilde;a y su confirmaci&oacute;n, no coinciden', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir una contrase&ntilde;a', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if((Ext.getCmp('tipo_usuario').getValue() != 'Egresado')&&(Ext.getCmp('tipo_usuario').getValue() != 'Monitor')&&(Ext.getCmp('tipo_usuario').getValue() != 'Admin'))
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de usuario v&aacute;lido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('usu_habilitado').checked == false && Ext.getCmp('usu_deshabilitado').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione el estado de la cuenta', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
					
	
		
	return salida;
	

}

<!-- 	 ESTE ES EL METODO QUE ME PERMITE MOSTRAR EL MENSAJE DE CONFIRMACION PARA EL BORRADO DE LOS USUARIOS EN EL GRIP -->
	function confirDelete1()
	{
		if(UsuariosListingEditorGrid.selModel.getCount() != 1)
		{
		//	Ext.MessageBox.confiirm('Confirmacion','Desea borrar este Usuario?', borrarUsuario);
			
			
			
Ext.Msg.show({
   title:'Save Changes?',
   msg: 'Desea borrar este Usuario?',
   buttons: Ext.Msg.YESNO,
   fn: borrarUsuario,
   animEl: 'elId',
   icon: Ext.MessageBox.QUESTION
});

			
		} else if(UsuariosListingEditorGrid.selModel.getCount() > 1){
			Ext.MessageBox.confirm('Confirmacion','Borrar estos Usuarios?', borrarUsuario);
		} else {
			Ext.MessageBox.alert('Advertencia','Usted no puede borrar un elemento que no ha sido seleccionado');
		}
	
	}
	

<!-- 	 ESTE ES EL METODO-->
	function borrarUsuario(btn){
		if(btn=='yes'){
			var selections = UsuariosListingEditorGrid.selModel.getSelections();
			var prez = [];
			fila	=	Ext.getCmp('listaPerfilEgresado').selModel.getSelected();
			identificador	=	fila.get('cedula');
			
					/*			Ext.Msg.show
					({
						title:'Error',
						msg: 'cc:'+identificador,
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});*/
					
			prez.push(identificador);
			var encoded_array = Ext.encode(prez);
			Ext.Ajax.request({  
				waitMsg: 'Por Favor Espere...',
   				url: 'perfilEgresado/cargar', 
				params: { 
					task: "DELETEUSU", 
					ids:  encoded_array
				}, 
				success: function(response){
          obj = Ext.util.JSON.decode(response.responseText);
					if (obj.success)
					{
						Ext.Msg.show
						({
							title:'Alerta!',
							msg: obj.mensaje,
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
						UsuariosDataStore.reload();
					}
					else if (obj.success == false)
					{
						Ext.MessageBox.show
						({
							title: 'Advertencia',
							msg: obj.errors.reason,
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.WARNING
						});
					}
				},
				failure: function(response){
					var result=response.responseText;
					Ext.Msg.show
					({
						title:'Error',
						msg: 'No se pudo conectar con la base de datos',
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});
				}
			});
		}
	}
  } 
  }
}();
