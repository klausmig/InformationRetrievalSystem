



var experienciaEgresado = function () 
{

return { 
		init: function () {
		
    	var UsuariosDataStore;
    	var UsuariosColumnModel;
    	var UsuariosListingEditorGrid;
    	var UsuariosListingWindow;
    	var usuariosHabilitados=true;
    	var experienciaesDataStore;
    	
    	var idExperiencia=0;
 
        Ext.form.VTypes.cedulaVal  = /^[0-9]+/;
		Ext.form.VTypes.cedulaMask = /[0-9]/;
		Ext.form.VTypes.cedulaText = 'C�dula Invalida.';
		Ext.form.VTypes.cedula 	= function(v){
			return Ext.form.VTypes.cedulaVal.test(v);
		};
             
	

    	UsuariosDataStore = new Ext.data.JsonStore({
    		id: 'UsuariosDataStore',
    		url:'experiencia/cargar',
    		root: 'results',
    		baseParams:{task:'LISTING'},
    		totalProperty: 'total',
    		fields:[
                
              
                {name: 'id', type: 'int'},
                {name: 'numero', type: 'int'},                
                {name: 'institucion', type: 'string'},
                {name: 'fecha_inicio', type: 'string'},
                {name: 'fecha_fin', type: 'string'},
                {name: 'logro', type: 'string'},
                {name: 'responsabilidad', type: 'string'}
    											
    		],
    		sortInfo:{field: 'numero', direction: 'ASC'}
    	});
    	
    	UsuariosDataStore.load({params: {start: 0, limit: 10}});
    
      
    	UsuariosColumnModel = new Ext.grid.ColumnModel(
    		[{
    			header: 'No.',
    			dataIndex: 'numero',
                sortable:true,
    			width: 50
    		},{
    			header: 'Instituci&oacute;n',
    			dataIndex: 'institucion',
                sortable:true,
    			width: 170
    		},{
    			header: 'Logro',
    			dataIndex: 'logro',
                sortable:true,
    			width: 105
    		},{
    			header: 'Responsabilidad',
    			dataIndex: 'responsabilidad',
                sortable:true,
    			width: 120
    		}
    	]);
    	
    	UsuariosColumnModel.defaultSortable= true;
    
    	UsuariosListingEditorGrid =  new Ext.grid.EditorGridPanel({
    		id: 'UsuariosListingEditorGrid',
    		store: UsuariosDataStore,
    		cm: UsuariosColumnModel,
    		enableColLock:true,
    		clicksToEdit:1,
    	
    		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
    	});
    
    	UsuariosListingWindow = new Ext.Panel({
    		id: 'UsuariosListingWindow',
    		title: 'Informacion de Usuarios',
    		closable:true,
    		width:100,
    		height:390,
    		plain:true,
    		layout: 'fit',
    		frame: true,
    		items: UsuariosListingEditorGrid
    	});



/*	experienciaesDataStore = new Ext.data.JsonStore({
		id: 'UsuariosDataStore',
		url:'usuarios/cargar',
		root: 'results',
		baseParams:{task:'LISTINGexperiencia'},
		totalProperty: 'total',
		fields:[
			{name: 'perId', type: 'string'},
			{name: 'perNombre', type: 'string'},
		],
		sortInfo:{field: 'perId', direction: 'ASC'}
	});
	experienciaesDataStore.load();*/

       

    
    	
    	var tipoExperiencia = [
    	['Doctorado'],
    	['Maestria'],
    	['Postgrado'],
    	['Pregrado'],
     	['Tecnologo']
    	];
  	


    	
    	var comboTipoExperiencia = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoExperiencia
    	});


    	
        var gridListadoexperiencia = new Ext.grid.GridPanel({
		//	xtype: 'grid',
			id:	'listaexperiencia',
			bbar: new Ext.PagingToolbar({
				pageSize: 10,
				store: UsuariosDataStore,
				displayInfo: true,
				displayMsg: 'Experiencia {0} - {1} de {2}',
				emptyMsg: "No hay Experiencias"
			}),
			ds: UsuariosDataStore,
			cm: UsuariosColumnModel,
			//width:400,
			sm: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {
  				rowselect: function(sm, row, rec) 
                {
                
                
                    myMask = new Ext.LoadMask(formularioExperiencia.getEl(), {msg:'Cargando...',removeMask: true});
                    myMask.show();
                    setTimeout('myMask.hide()',500);
                    Ext.getCmp('experienciaPanelDatos').setDisabled(false);
                    //	Ext.getCmp('exp_lugar').setValue('Hola');
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg:	Ext.getCmp('experienciaPanelDatos').getId(), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    formularioExperiencia.getForm().loadRecord(rec);
                    var data =rec.data;
                    idExperiencia=data.id;
                    
                    
                    
                    nombreU = Ext.getCmp('exp_institucion').getValue();
                    
                    
                    titulo = 'Actualizar Experiencia en "'+nombreU+'"';
                    
                    Ext.getCmp('experienciaPanelDatos').setTitle(titulo);
    				
    				Ext.getCmp('exp_btGuardar').setText('Actualizar');
    				

    
    				  //Ext.getCmp('empleado').setValue(true);
    				
    					Ext.getCmp('exp_btGuardar').setDisabled(false);
    					Ext.getCmp('exp_btLimpiar').setDisabled(false);
                    
                        				
                    //	Ext.getCmp('cedula').setDisabled(false);
                    //   Ext.getCmp('tipo_identificacion').setDisabled(false);
                    //
                    // Ext.getCmp('exp_sexo_m_registro').setValue(true);
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                    //	Ext.getCmp('clave').setDisabled(false);
                    //	Ext.getCmp('reClave').setDisabled(false);
                    //	Ext.getCmp('login').setDisabled(false);
                    //	Ext.getCmp('tipo_usuario').setDisabled(false);
                    
                    
                    
                    //	Ext.getCmp('reClave').setValue(rec.get('clave'));
                    
                    
                    //	Ext.getCmp('radios_estado').setDisabled(false);
                    //	Ext.getCmp('usu_habilitado').setValue(true);
                    
                    //	Ext.getCmp('nombre').setValue('sexo:'+rec.get('sexo'));
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg: rec.get('sexo'), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    
                    
                    
                    //	Ext.getCmp('sexo').setValue(rec.get('sexo'));    				
        }
			}
		}),
		//bodyStyle:'padding:10px',
		height: 400,
		title:'Lista de Experiencias',
		frame:true,
		border: true,
		        listeners: {
		        	render: function(g) {
		        		g.getSelectionModel().selectRow(0);
		        	},
		        	delay: 10
		        },
			tbar: [
            
           
            {
				text: 'Nuevo',
				tooltip: 'Agregar un usuario',
				handler: function()
				{
					myMask = new Ext.LoadMask(formularioExperiencia.getEl(), {msg:'Cargando...',removeMask: true});
					myMask.show();
					setTimeout('myMask.hide()',500);
					
					Ext.getCmp('experienciaPanelDatos').setDisabled(false);
					
					titulo = 'Nueva Experiencia';
					Ext.getCmp('experienciaPanelDatos').setTitle(titulo);
					formularioExperiencia.getForm().reset();
					

                
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                   // Ext.getCmp('clave').setDisabled(false);
                  //  Ext.getCmp('reClave').setDisabled(false);
                  //  Ext.getCmp('login').setDisabled(false);
                  //  Ext.getCmp('tipo_usuario').setDisabled(false);
                    Ext.getCmp('exp_btGuardar').setDisabled(false);
                	Ext.getCmp('exp_btLimpiar').setDisabled(false);
                    
                    Ext.getCmp('exp_btGuardar').setText('Crear');
				

				},
				iconCls:'add_user',
				xtype: 'tbbutton'
			},{
				text: 'Borrar',
				tooltip: 'Borra un usuario',
				handler: confirDelete1,
				iconCls:'del_user'
			}],
			plugins:[new Ext.ux.grid.Search({
				mode:          'local',
				position:      top,
				searchText:    'Filtrar',
				iconCls:  'buscar',
				selectAllText: 'Seleccionar todos',
				searchTipText: 'Escriba el texto que desea buscar y presione la tecla enter',
				width:         250
			})]
		});
		
    
            
  

    
    
      var tab2 = {
       xtype: 'panel',
        labelAlign: 'top',
      //  title: 'Inner Tabs',
        bodyStyle:'padding:5px',
        width: 500,
        height:600,
        x:5,
        y:0,
        layout:'absolute',
        items: [{
            layout:'column',
            x:5,
            y:5,
            width: 500,
            border:false,
            items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'Nombre de la Instituci&oacute;n',
                    name: 'institucion',
                    id: 'exp_institucion',
                    anchor:'95%'
                }]
            },{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items:[{
						fieldLabel: 'Fecha de Inicio',
                        xtype:'datefield',
						name: 'fecha_inicio',
						id: 'exp_fecha_inicio',
                        format : "Y-m-d",
                        value:'A�o-Mes-D�a',
						allowBlank:true,
						anchor:'95%'
				
					}, {
						fieldLabel: 'Fecha Fin',
                        xtype:'datefield',
						name: 'fecha_fin',
						id: 'exp_fecha_fin',
                        format : "Y-m-d",
                        value:'A�o-Mes-D�a',
						allowBlank:true,
						anchor:'95%'
				
					}]
      
            }]},
            
            {
            layout:'form',
            x:5,
            y:100,
            width: 500,
            border:false,
            items:[{
                
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textarea',
                    fieldLabel: 'Logro Obtenido',
                    name: 'logro',
                    id: 'exp_logro',
                    anchor:'98%'
                }]}]
            },
            
            {
            layout:'form',
            x:5,
            y:220,
            width: 500,
            border:false,
            items:[{
                
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textarea',
                    fieldLabel: 'Principal Responsabilidad',
                    name: 'responsabilidad',
                    id: 'exp_responsabilidad',
                    anchor:'98%'
                }]}]
            }            


]
    };
 
              		
	var formularioExperiencia = new Ext.FormPanel({
        	id: 'formExperiencia',
        	url: 'experiencia/cargar',
        	frame: true,
        	labelAlign: 'left',
        	title: 'Gestion De Experiencias',
        	bodyStyle:'padding:5px',
        	width: 1050,
            //autowidth:true,
        	height: 500,
        	autoScroll:true,
        	layout: 'absolute',
        	items: [{
        		x:5,
        		y:0,
        		layout: 'fit',
        		autoScroll:true,
        		items: gridListadoexperiencia
                },{
        	//	columnWidth: 0.57,
        		x:490,
        		y:0,
        		xtype:  'panel',
        		id:'experienciaPanelDatos',
        		disabled:true,      	
        		title:'Actualizar Datos de Experiencias',
        		defaultType: 'textfield',
        	    height: 400,
        	  
        		
        		frame:true,
        		layout: 'absolute',
        	//	bodyStyle: Ext.isIE ? 'padding:0 0 20px 15px;' : 'padding:10px 15px;',
        	    //bodyStyle:'padding:10px',
                width: 530,
        
        	/*	style: {
        			"margin-left": "10px", // when you add custom margin in IE 6...
        			"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        		}, */
        
                    //items:[panelInfoPersonal,detalleRegistro],
                    items:[tab2],
                    
        			buttons:[
                			{
                			text:'Actualizar',
                			formBind: true,
                			align:'center',
                			id: 'exp_btGuardar',
                			iconCls:'salvar',
                			disabled:true,
                			handler:function(formulario,accion)
                			{
                			var verificacion =  'almacenar';
                            
                            //verificarCampos();
                			if(verificacion  ==  'almacenar')
                			{
                			/*	var contrasenaEncrypt = '';
                				if(Ext.getCmp('usu_clave').getValue() != ''){
                				contrasenaEncrypt = hex_md5(Ext.getCmp('usu_clave').getValue());
                				}*/
                
                			/*	var sexo = 'm';
                				if(Ext.getCmp('sexo_f_registro').checked){
                				    sexo = 'f';
                				}
                				*/
                				if (Ext.getCmp('exp_btGuardar').getText() == 'Actualizar')
                				{
                					task = 'UPDATEEST';
                				//	Ext.getCmp('usu_id').setDisabled(false);
                				}
                				else
                				{
                					task = 'CREATEEST';
                				}
                				
                				//Ext.MessageBox.alert('Advertencia','ok');
                			//	Ext.getCmp('exp_lugar').setDisabled(false);
                			//	Ext.getCmp('tipo_experiencia').setDisabled(false);
                				
                				
                				formularioExperiencia.getForm().submit({
                				
                    				method: 'POST',
                    				params: {task: task,id:idExperiencia},
                    				waitTitle: 'Enviando',
                    				waitMsg: 'Enviando datos...',
                    				success: function(response, action)
                    				{
                                        obj = Ext.util.JSON.decode(action.response.responseText);
                                        Ext.Msg.show
                                        ({
                                            title:'Alerta!',
                                            msg: obj.mensaje,
                                            buttons: Ext.Msg.OK,
                                            animEl: 'elId',
                                            icon: Ext.MessageBox.INFO
                                        });
                                        UsuariosDataStore.reload();
                                     //   Ext.getCmp('exp_lugar').setDisabled(true);
                                     //   Ext.getCmp('tipo_experiencia').setDisabled(true);
                    				},
                    				failure: function(form, action, response)
                    				{
                        			//	Ext.getCmp('exp_lugar').setDisabled(true);
                                     //   Ext.getCmp('tipo_experiencia').setDisabled(true);
                                        
                        				if(action.failureType == 'server'){
                        					obj = Ext.util.JSON.decode(action.response.responseText); 
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: obj.errors.reason,
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        				}
                        				else 
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: 'No se pudo procesar el Script en el servidor',
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        			 
                        			//	Ext.getCmp('usu_contrasena').setDisabled(false);
                        			//	Ext.getCmp('usu_recontrasena').setDisabled(false);
                        				}
                				});
                			}
                			}
                			},{
                			text:'Limpiar',
                			align:'center',
                			formBind: true,
                			id: 'exp_btLimpiar',
                			iconCls:'limpiar',
                			disabled:true,
                			handler:function()
                				{ 
   
                					formularioExperiencia.getForm().reset();

                					
                				}
                			}
        			]
        
        		}],
        		renderTo: 'experiencia-perfil'
	});
	
var verificarCampos=function ()

{

		salida =  'almacenar';
		

		if(Ext.getCmp('nombre').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un nombre', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('nombre').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('nombre').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un apellido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('apellido').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('sexo_m_registro').checked == false && Ext.getCmp('sexo_f_registro').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione un Sexo', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir el n&uacute;mero de c&eacute;dula', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue().length < 8 || Ext.getCmp('cedula').getValue().length > 10 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe estar en un rango de 8 a 10 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('cedula').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe ser n�merica', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('tipo_identificacion').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de identificaci&oacute;n', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('tipo_identificacion').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El tipo no debe tener caracteres num&eacute;ricos ', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('telefono').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un t&eacute;lefono', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('telefono').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El t&eacute;lefono debe ser num&eacute;rico', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un usuario', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue().length < 5 && Ext.getCmp('login').getValue().length > 20 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El usuario debe estar en un rango de 5 a 20 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() != Ext.getCmp('reClave').getValue()) 
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La contrase&ntilde;a y su confirmaci&oacute;n, no coinciden', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir una contrase&ntilde;a', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if((Ext.getCmp('tipo_usuario').getValue() != 'Egresado')&&(Ext.getCmp('tipo_usuario').getValue() != 'Monitor')&&(Ext.getCmp('tipo_usuario').getValue() != 'Admin'))
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de usuario v&aacute;lido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('usu_habilitado').checked == false && Ext.getCmp('usu_deshabilitado').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione el estado de la cuenta', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
					
	
		
	return salida;
	

}

<!-- 	 ESTE ES EL METODO QUE ME PERMITE MOSTRAR EL MENSAJE DE CONFIRMACION PARA EL BORRADO DE LOS USUARIOS EN EL GRIP -->
	function confirDelete1()
	{
		if(UsuariosListingEditorGrid.selModel.getCount() != 1)
		{
		//	Ext.MessageBox.confiirm('Confirmacion','Desea borrar este Usuario?', borrarUsuario);
			
			
			
            Ext.Msg.show({
               title:'Save Changes?',
               msg: 'Desea borrar esta Experiencia Laboral?',
               buttons: Ext.Msg.YESNO,
               fn: borrarUsuario,
               animEl: 'elId',
               icon: Ext.MessageBox.QUESTION
            });

			
		} else if(UsuariosListingEditorGrid.selModel.getCount() > 1){
			Ext.MessageBox.confirm('Confirmacion','Borrar estos Experiencias?', borrarUsuario);
		} else {
			Ext.MessageBox.alert('Advertencia','Usted no puede borrar un elemento que no ha sido seleccionado');
		}
	
	}
	

<!-- 	 ESTE ES EL METODO-->
	function borrarUsuario(btn){
		if(btn=='yes'){
			var selections = UsuariosListingEditorGrid.selModel.getSelections();
			var prez = [];
			fila	=	Ext.getCmp('listaexperiencia').selModel.getSelected();
			identificador	=	fila.get('id');
			
					/*			Ext.Msg.show
					({
						title:'Error',
						msg: 'cc:'+identificador,
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});*/
					
			prez.push(identificador);
			var encoded_array = Ext.encode(prez);
			Ext.Ajax.request({  
				waitMsg: 'Por Favor Espere...',
   				url: 'experiencia/cargar', 
				params: { 
					task: "DELETEEST", 
					ids:  encoded_array
				}, 
				success: function(response){
          obj = Ext.util.JSON.decode(response.responseText);
					if (obj.success)
					{
						Ext.Msg.show
						({
							title:'Alerta!',
							msg: obj.mensaje,
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
						
                        formularioExperiencia.getForm().reset();
                       // experienciaPanelDatos.setDisabled(true);
						UsuariosDataStore.reload();
					}
					else if (obj.success == false)
					{
						Ext.MessageBox.show
						({
							title: 'Advertencia',
							msg: obj.errors.reason,
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.WARNING
						});
					}
				},
				failure: function(response){
					var result=response.responseText;
					Ext.Msg.show
					({
						title:'Error',
						msg: 'No se pudo conectar con la base de datos',
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});
				}
			});
		}
	}
  } 
  }
}();
