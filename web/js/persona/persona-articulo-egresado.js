



var articuloEgresado = function () 
{

return { 
		init: function () {
		
    	var UsuariosDataStore;
    	var UsuariosColumnModel;
    	var UsuariosListingEditorGrid;
    	var UsuariosListingWindow;
    	var usuariosHabilitados=true;
    	var articuloesDataStore;
    	
    	var idArticulo=0;
 
        Ext.form.VTypes.cedulaVal  = /^[0-9]+/;
		Ext.form.VTypes.cedulaMask = /[0-9]/;
		Ext.form.VTypes.cedulaText = 'C�dula Invalida.';
		Ext.form.VTypes.cedula 	= function(v){
			return Ext.form.VTypes.cedulaVal.test(v);
		};
             
	

    	UsuariosDataStore = new Ext.data.JsonStore({
    		id: 'UsuariosDataStore',
    		url:'articulo/cargar',
    		root: 'results',
    		baseParams:{task:'LISTING'},
    		totalProperty: 'total',
    		fields:[
                
              
                {name: 'id', type: 'int'},
                {name: 'numero', type: 'int'},
                {name: 'titulo', type: 'string'},
                {name: 'anno', type: 'string'},
                {name: 'articulo', type: 'string'}
    											
    		],
    		sortInfo:{field: 'numero', direction: 'ASC'}
    	});
    	
    	UsuariosDataStore.load({params: {start: 0, limit: 10}});
    
      
    	UsuariosColumnModel = new Ext.grid.ColumnModel(
    		[{
    			header: 'No.',
    			dataIndex: 'numero',
                sortable:true,
    			width: 50
    		},{
    			header: 'T&iacute;tulo',
    			dataIndex: 'titulo',
                sortable:true,
    			width: 170
    		},{
    			header: 'A&ntilde;o',
    			dataIndex: 'anno',
                sortable:true,
    			width: 105
    		},{
    			header: 'Descripci&oacute;n',
    			dataIndex: 'articulo',
                sortable:true,
    			width: 120
    		}
    	]);
    	
    	UsuariosColumnModel.defaultSortable= true;
    
    	UsuariosListingEditorGrid =  new Ext.grid.EditorGridPanel({
    		id: 'UsuariosListingEditorGrid',
    		store: UsuariosDataStore,
    		cm: UsuariosColumnModel,
    		enableColLock:true,
    		clicksToEdit:1,
    	
    		selModel: new Ext.grid.RowSelectionModel({singleSelect:false})
    	});
    
    	UsuariosListingWindow = new Ext.Panel({
    		id: 'UsuariosListingWindow',
    		title: 'Informacion de Usuarios',
    		closable:true,
    		width:100,
    		height:390,
    		plain:true,
    		layout: 'fit',
    		frame: true,
    		items: UsuariosListingEditorGrid
    	});



/*	articuloesDataStore = new Ext.data.JsonStore({
		id: 'UsuariosDataStore',
		url:'usuarios/cargar',
		root: 'results',
		baseParams:{task:'LISTINGarticulo'},
		totalProperty: 'total',
		fields:[
			{name: 'perId', type: 'string'},
			{name: 'perNombre', type: 'string'},
		],
		sortInfo:{field: 'perId', direction: 'ASC'}
	});
	articuloesDataStore.load();*/

       

    
    	
    	var tipoArticulo = [
    	['Doctorado'],
    	['Maestria'],
    	['Postgrado'],
    	['Pregrado'],
     	['Tecnologo']
    	];
  	


    	
    	var comboTipoArticulo = new Ext.data.SimpleStore({
    	fields: ['opciones'],
    	data  : tipoArticulo
    	});


    	
        var gridListadoarticulo = new Ext.grid.GridPanel({
		//	xtype: 'grid',
			id:	'listaarticulo',
			bbar: new Ext.PagingToolbar({
				pageSize: 10,
				store: UsuariosDataStore,
				displayInfo: true,
				displayMsg: 'Articulos {0} - {1} de {2}',
				emptyMsg: "No hay Articulos"
			}),
			ds: UsuariosDataStore,
			cm: UsuariosColumnModel,
			//width:400,
			sm: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners: {
  				rowselect: function(sm, row, rec) 
                {
                
                
                    myMask = new Ext.LoadMask(formularioArticulo.getEl(), {msg:'Cargando...',removeMask: true});
                    myMask.show();
                    setTimeout('myMask.hide()',500);
                    Ext.getCmp('articuloPanelDatos').setDisabled(false);
                    //	Ext.getCmp('art_lugar').setValue('Hola');
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg:	Ext.getCmp('articuloPanelDatos').getId(), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    formularioArticulo.getForm().loadRecord(rec);
                    var data =rec.data;
                    idArticulo=data.id;
                    
                    
                    
                    nombreU = Ext.getCmp('art_titulo').getValue();
                    
                    
                    articulo = 'Actualizar Art&iacute;culo "'+nombreU+'"';
                    
                    Ext.getCmp('articuloPanelDatos').setTitle(articulo);
    				
    				Ext.getCmp('art_btGuardar').setText('Actualizar');
    				

    
    				  //Ext.getCmp('empleado').setValue(true);
    				
    					Ext.getCmp('art_btGuardar').setDisabled(false);
    					Ext.getCmp('art_btLimpiar').setDisabled(false);
                    
                        				
                    //	Ext.getCmp('cedula').setDisabled(false);
                    //   Ext.getCmp('tipo_identificacion').setDisabled(false);
                    //
                    // Ext.getCmp('art_sexo_m_registro').setValue(true);
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                    //	Ext.getCmp('clave').setDisabled(false);
                    //	Ext.getCmp('reClave').setDisabled(false);
                    //	Ext.getCmp('login').setDisabled(false);
                    //	Ext.getCmp('tipo_usuario').setDisabled(false);
                    
                    
                    
                    //	Ext.getCmp('reClave').setValue(rec.get('clave'));
                    
                    
                    //	Ext.getCmp('radios_estado').setDisabled(false);
                    //	Ext.getCmp('usu_habilitado').setValue(true);
                    
                    //	Ext.getCmp('nombre').setValue('sexo:'+rec.get('sexo'));
                    //	Ext.MessageBox.show({ title: 'Advertencia', msg: rec.get('sexo'), buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
                    
                    
                    
                    //	Ext.getCmp('sexo').setValue(rec.get('sexo'));    				
        }
			}
		}),
		//bodyStyle:'padding:10px',
		height: 400,
		title:'Lista de Articulos',
		frame:true,
		border: true,
		        listeners: {
		        	render: function(g) {
		        		g.getSelectionModel().selectRow(0);
		        	},
		        	delay: 10
		        },
			tbar: [
            
           
            {
				text: 'Nuevo',
				tooltip: 'Agregar un usuario',
				handler: function()
				{
					myMask = new Ext.LoadMask(formularioArticulo.getEl(), {msg:'Cargando...',removeMask: true});
					myMask.show();
					setTimeout('myMask.hide()',500);
					
					Ext.getCmp('articuloPanelDatos').setDisabled(false);
					
					articulo = 'Nuevo Articulo';
					Ext.getCmp('articuloPanelDatos').setTitle(articulo);
					formularioArticulo.getForm().reset();
					

                
                    
                    //Ext.getCmp('usu_login').setValue(rec.get('usuId'));
                    //Ext.getCmp('usu_login').setDisabled(true);
                    
                   // Ext.getCmp('clave').setDisabled(false);
                  //  Ext.getCmp('reClave').setDisabled(false);
                  //  Ext.getCmp('login').setDisabled(false);
                  //  Ext.getCmp('tipo_usuario').setDisabled(false);
                    Ext.getCmp('art_btGuardar').setDisabled(false);
                	Ext.getCmp('art_btLimpiar').setDisabled(false);
                    
                    Ext.getCmp('art_btGuardar').setText('Crear');
				

				},
				iconCls:'add_user',
				xtype: 'tbbutton'
			},{
				text: 'Borrar',
				tooltip: 'Borra un usuario',
				handler: confirDelete1,
				iconCls:'del_user'
			}],
			plugins:[new Ext.ux.grid.Search({
				mode:          'local',
				position:      top,
				searchText:    'Filtrar',
				iconCls:  'buscar',
				selectAllText: 'Seleccionar todos',
				searchTipText: 'Escriba el texto que desea buscar y presione la tecla enter',
				width:         250
			})]
		});
		
    
            
  

    
    
      var tab2 = {
       xtype: 'panel',
        labelAlign: 'top',
      //  title: 'Inner Tabs',
        bodyStyle:'padding:5px',
        width: 500,
        height:600,
        x:5,
        y:0,
        layout:'absolute',
        items: [{
            layout:'column',
            x:5,
            y:5,
            width: 500,
            border:false,
            items:[{
                columnWidth:.5,
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textfield',
                    fieldLabel: 'Titulo del Art&iacute;culo',
                    name: 'titulo',
                    id: 'art_titulo',
                    anchor:'95%'
                }, {
						fieldLabel: 'A&ntilde;o',
                        xtype:'datefield',
						name: 'anno',
						id: 'art_anno',
                        format : "Y",
                        value:'A�o',
						allowBlank:true,
						anchor:'95%'
					
                }]
            }]},
            
            {
            layout:'form',
            x:5,
            y:120,
            width: 500,
            border:false,
            items:[{
                
                layout: 'form',
                border:false,
                items: [{
                    xtype:'textarea',
                    fieldLabel: 'Descripci&oacute;n del Art&iacute;culo',
                    name: 'articulo',
                    id: 'art_articulo',
                    anchor:'98%'
                }]}]
            }


]
    };
 
              		
	var formularioArticulo = new Ext.FormPanel({
        	id: 'formArticulo',
        	url: 'articulo/cargar',
        	frame: true,
        	labelAlign: 'left',
        	title: 'Gestion De Articulos',
        	bodyStyle:'padding:5px',
        	width: 1050,
            //autowidth:true,
        	height: 500,
        	autoScroll:true,
        	layout: 'absolute',
        	items: [{
        		x:5,
        		y:0,
        		layout: 'fit',
        		autoScroll:true,
        		items: gridListadoarticulo
                },{
        	//	columnWidth: 0.57,
        		x:490,
        		y:0,
        		xtype:  'panel',
        		id:'articuloPanelDatos',
        		disabled:true,      	
        		title:'Actualizar Datos de Articulos',
        		defaultType: 'textfield',
        	    height: 310,
        	  
        		
        		frame:true,
        		layout: 'absolute',
        	//	bodyStyle: Ext.isIE ? 'padding:0 0 20px 15px;' : 'padding:10px 15px;',
        	    //bodyStyle:'padding:10px',
                width: 530,
        
        	/*	style: {
        			"margin-left": "10px", // when you add custom margin in IE 6...
        			"margin-right": Ext.isIE6 ? (Ext.isStrict ? "-10px" : "-13px") : "0"  // you have to adjust for it somewhere else
        		}, */
        
                    //items:[panelInfoPersonal,detalleRegistro],
                    items:[tab2],
                    
        			buttons:[
                			{
                			text:'Actualizar',
                			formBind: true,
                			align:'center',
                			id: 'art_btGuardar',
                			iconCls:'salvar',
                			disabled:true,
                			handler:function(formulario,accion)
                			{
                			var verificacion =  'almacenar';
                            
                            //verificarCampos();
                			if(verificacion  ==  'almacenar')
                			{
                			/*	var contrasenaEncrypt = '';
                				if(Ext.getCmp('usu_clave').getValue() != ''){
                				contrasenaEncrypt = hex_md5(Ext.getCmp('usu_clave').getValue());
                				}*/
                
                			/*	var sexo = 'm';
                				if(Ext.getCmp('sexo_f_registro').checked){
                				    sexo = 'f';
                				}
                				*/
                				if (Ext.getCmp('art_btGuardar').getText() == 'Actualizar')
                				{
                					task = 'UPDATEEST';
                				//	Ext.getCmp('usu_id').setDisabled(false);
                				}
                				else
                				{
                					task = 'CREATEEST';
                				}
                				
                				//Ext.MessageBox.alert('Advertencia','ok');
                			//	Ext.getCmp('art_lugar').setDisabled(false);
                			//	Ext.getCmp('tipo_articulo').setDisabled(false);
                				
                				
                				formularioArticulo.getForm().submit({
                				
                    				method: 'POST',
                    				params: {task: task,id:idArticulo},
                    				waitTitle: 'Enviando',
                    				waitMsg: 'Enviando datos...',
                    				success: function(response, action)
                    				{
                                        obj = Ext.util.JSON.decode(action.response.responseText);
                                        Ext.Msg.show
                                        ({
                                            title:'Alerta!',
                                            msg: obj.mensaje,
                                            buttons: Ext.Msg.OK,
                                            animEl: 'elId',
                                            icon: Ext.MessageBox.INFO
                                        });
                                        UsuariosDataStore.reload();
                                     //   Ext.getCmp('art_lugar').setDisabled(true);
                                     //   Ext.getCmp('tipo_articulo').setDisabled(true);
                    				},
                    				failure: function(form, action, response)
                    				{
                        			//	Ext.getCmp('art_lugar').setDisabled(true);
                                     //   Ext.getCmp('tipo_articulo').setDisabled(true);
                                        
                        				if(action.failureType == 'server'){
                        					obj = Ext.util.JSON.decode(action.response.responseText); 
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: obj.errors.reason,
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        				}
                        				else 
                        					Ext.Msg.show
                        					({
                        						title:'Error',
                        						msg: 'No se pudo procesar el Script en el servidor',
                        						buttons: Ext.Msg.OK,
                        						animEl: 'elId',
                        						icon: Ext.MessageBox.ERROR
                        					});
                        			 
                        			//	Ext.getCmp('usu_contrasena').setDisabled(false);
                        			//	Ext.getCmp('usu_recontrasena').setDisabled(false);
                        				}
                				});
                			}
                			}
                			},{
                			text:'Limpiar',
                			align:'center',
                			formBind: true,
                			id: 'art_btLimpiar',
                			iconCls:'limpiar',
                			disabled:true,
                			handler:function()
                				{ 
   
                					formularioArticulo.getForm().reset();

                					
                				}
                			}
        			]
        
        		}],
        		renderTo: 'articulo-perfil'
	});
	
var verificarCampos=function ()

{

		salida =  'almacenar';
		

		if(Ext.getCmp('nombre').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un nombre', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('nombre').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('nombre').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El nombre no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un apellido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('apellido').getValue().length > 50 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido supera el rango minimo de 50 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('apellido').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El apellido no debe tener caracteres num&eacute;ricos', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('sexo_m_registro').checked == false && Ext.getCmp('sexo_f_registro').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione un Sexo', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir el n&uacute;mero de c&eacute;dula', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('cedula').getValue().length < 8 || Ext.getCmp('cedula').getValue().length > 10 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe estar en un rango de 8 a 10 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(isNaN(Ext.getCmp('cedula').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La c&eacute;dula debe ser n�merica', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('tipo_identificacion').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de identificaci&oacute;n', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('tipo_identificacion').getValue()) == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El tipo no debe tener caracteres num&eacute;ricos ', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('telefono').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un t&eacute;lefono', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if(isNaN(Ext.getCmp('telefono').getValue()) == true){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El t&eacute;lefono debe ser num&eacute;rico', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir un usuario', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('login').getValue().length < 5 && Ext.getCmp('login').getValue().length > 20 ){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'El usuario debe estar en un rango de 5 a 20 caracteres', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() != Ext.getCmp('reClave').getValue()) 
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'La contrase&ntilde;a y su confirmaci&oacute;n, no coinciden', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('clave').getValue() == ''){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe escribir una contrase&ntilde;a', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}

		else if((Ext.getCmp('tipo_usuario').getValue() != 'Egresado')&&(Ext.getCmp('tipo_usuario').getValue() != 'Monitor')&&(Ext.getCmp('tipo_usuario').getValue() != 'Admin'))
		{
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Debe seleccionar un tipo de usuario v&aacute;lido', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
		else if(Ext.getCmp('usu_habilitado').checked == false && Ext.getCmp('usu_deshabilitado').checked == false){
		Ext.MessageBox.show({ title: 'Advertencia', msg: 'Seleccione el estado de la cuenta', buttons: Ext.MessageBox.OK, animEl: 'mb9', icon:Ext.MessageBox.WARNING});
		return false;
		}
					
	
		
	return salida;
	

}

<!-- 	 ESTE ES EL METODO QUE ME PERMITE MOSTRAR EL MENSAJE DE CONFIRMACION PARA EL BORRADO DE LOS USUARIOS EN EL GRIP -->
	function confirDelete1()
	{
		if(UsuariosListingEditorGrid.selModel.getCount() != 1)
		{
		//	Ext.MessageBox.confiirm('Confirmacion','Desea borrar este Usuario?', borrarUsuario);
			
			
			
            Ext.Msg.show({
               title:'Save Changes?',
               msg: 'Desea borrar este Art&iacute;culo?',
               buttons: Ext.Msg.YESNO,
               fn: borrarUsuario,
               animEl: 'elId',
               icon: Ext.MessageBox.QUESTION
            });

			
		} else if(UsuariosListingEditorGrid.selModel.getCount() > 1){
			Ext.MessageBox.confirm('Confirmacion','Borrar estos Articulos?', borrarUsuario);
		} else {
			Ext.MessageBox.alert('Advertencia','Usted no puede borrar un elemento que no ha sido seleccionado');
		}
	
	}
	

<!-- 	 ESTE ES EL METODO-->
	function borrarUsuario(btn){
		if(btn=='yes'){
			var selections = UsuariosListingEditorGrid.selModel.getSelections();
			var prez = [];
			fila	=	Ext.getCmp('listaarticulo').selModel.getSelected();
			identificador	=	fila.get('id');
			
					/*			Ext.Msg.show
					({
						title:'Error',
						msg: 'cc:'+identificador,
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});*/
					
			prez.push(identificador);
			var encoded_array = Ext.encode(prez);
			Ext.Ajax.request({  
				waitMsg: 'Por Favor Espere...',
   				url: 'articulo/cargar', 
				params: { 
					task: "DELETEEST", 
					ids:  encoded_array
				}, 
				success: function(response){
          obj = Ext.util.JSON.decode(response.responseText);
					if (obj.success)
					{
						Ext.Msg.show
						({
							title:'Alerta!',
							msg: obj.mensaje,
							buttons: Ext.Msg.OK,
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});
						
                        formularioArticulo.getForm().reset();
                       // articuloPanelDatos.setDisabled(true);
						UsuariosDataStore.reload();
					}
					else if (obj.success == false)
					{
						Ext.MessageBox.show
						({
							title: 'Advertencia',
							msg: obj.errors.reason,
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.WARNING
						});
					}
				},
				failure: function(response){
					var result=response.responseText;
					Ext.Msg.show
					({
						title:'Error',
						msg: 'No se pudo conectar con la base de datos',
						buttons: Ext.Msg.OK,
						animEl: 'elId',
						icon: Ext.MessageBox.ERROR
					});
				}
			});
		}
	}
  } 
  }
}();
