<?php


/**
 * This class adds structure of 'sipel_articulo' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 12/22/09 18:40:57
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class ArticuloMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.ArticuloMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(ArticuloPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(ArticuloPeer::TABLE_NAME);
		$tMap->setPhpName('Articulo');
		$tMap->setClassname('Articulo');

		$tMap->setUseIdGenerator(true);

		$tMap->addPrimaryKey('ID_ARTICULO', 'IdArticulo', 'INTEGER', true, null);

		$tMap->addForeignKey('PERSONA_ID', 'PersonaId', 'INTEGER', 'sipel_persona', 'ID_PERSONA', true, null);

		$tMap->addColumn('NUMERO', 'Numero', 'INTEGER', false, null);

		$tMap->addColumn('TITULO', 'Titulo', 'VARCHAR', false, 50);

		$tMap->addColumn('ANNO', 'Anno', 'VARCHAR', false, 12);

		$tMap->addColumn('ARTICULO', 'Articulo', 'LONGVARCHAR', false, null);

	} // doBuild()

} // ArticuloMapBuilder
