# SIPEL INSTALLATION MANUAL :

# REQUERIMENTS:

- Symfony 1.2.9
- ExtJS 3.0.3
- PHP 5+
- MySql 5+

# NOTE: 
All Symfony commands have to be executed within the project folder, in this case the folder SIPEL.

# INSTALLATION IN THE LAB

- ssh vmlabs04
- create and change to the folder public_html/SIPEL
- symfony init-project SIPEL

# Main application:(authentication modules)

- symfony init-app main
- symfony init-module main autenticacion

# Admin appplication:

- symfony init-app admin
- symfony init-module persona principal
- buscarEgresado
- buscarMonitor
- perfilEgresado
- perfilMonitor
- usuarioPersona


# Persona application:

- symfony init-app persona
- symfony init-module persona egresado
- monitor
- articulo
- capacitacion
- estudio
- experiencia
- perfilMonitor
- perfilEgresado
- upload


- symfony configure:database "mysql:host=localhost;dbname=sipel" sipel sipel
- Within the web folder execute the command mv .htaccess htaccess

CHMOD 755 -R SIPEL

	4= lectura
	2= escritura
	1= ejecución

	6 (4+2)= lectura y escritura
	5 (4+1)= lectura y ejecución
	3 (2+1)= escritura y ejecución
	7 (4+2+1)= lectura, escritura y ejecución

Todo ello para los tres tipos de usuario.

Asi, un chmod file 777 significa que owner, group y others tienen permiso de lectura, escritura y ejecución

Copy:

config/schema.yml
config/empresa-schema.yml
web/css
web/images
web/js
web/parser
web/hv
data/fixtures/fixtures.yml

All modules of the admin, persona, main applications

- symfony propel:build-sql
- symfony propel:insert-sql --no-confirmation
- symfony propel:build-model
- symfony propel:data-load













