<?php

require_once 'C:\xampplite\php\PEAR\symfony/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

define('SIPEL_URL','http://ingreso.net/');


class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    // for compatibility / remove and enable only the plugins you want
    $this->enableAllPluginsExcept(array('sfDoctrinePlugin', 'sfCompat10Plugin'));
  }
}
