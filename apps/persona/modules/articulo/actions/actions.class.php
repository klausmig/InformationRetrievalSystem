<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class articuloActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarArticuloes();
				break;

			case "UPDATEEST":
				$salida = $this->updateArticulo();
				break;
			case "CREATEEST":
				$salida = $this->crearArticulo();
				break;
			case "DELETEEST":
				$salida = $this->deleteArticulo();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarArticuloes()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
	      
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
        //$idPersona=6;
	    
                
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		
		$conexion->add(ArticuloPeer::PERSONA_ID, $idPersona);
		$conexion->addAscendingOrderByColumn(ArticuloPeer::NUMERO);

		$estudios = ArticuloPeer::doSelect($conexion);
		
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($estudios As $estudio)
		{

          

          
            //DATOS ESTUDIOS
            $datos[$pos]['id']=$estudio->getIdArticulo();
            $datos[$pos]['numero']=$estudio->getNumero();
            $datos[$pos]['titulo']=$estudio->getTitulo();
            $datos[$pos]['anno']=$estudio->getAnno();
            $datos[$pos]['articulo']=$estudio->getArticulo();
		   
          
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}



		


	protected function  crearArticulo()
    {
        $salida	='';

       		
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
     //   $idPersona=6;
	    

		$salida	='';
	
        		
		if($idPersona)
		{
		
		
		
		    $conexion = new Criteria();
            $conexion->add(ArticuloPeer::PERSONA_ID, $idPersona);
            $conexion->addDescendingOrderByColumn(ArticuloPeer::NUMERO);
            $conexion->setLimit(1);
            
            $articulo = ArticuloPeer::doSelectOne($conexion);	
            
            if($articulo)
    		        $numero=$articulo->getNumero();
            else $numero=0;
            
            $numero++;	    

		    
		
            $estudio = new Articulo();
            
            
            $estudio->setPersonaId($idPersona);
            
            $estudio->setNumero($numero);
              			  
            $estudio->setTitulo($this->getRequestParameter('titulo'));

            $estudio->setAnno($this->getRequestParameter('anno'));
            

            $estudio->setArticulo($this->getRequestParameter('articulo'));
			 
			  

              
              
        try
        {
          $estudio->save();
        }
        catch (Exception $excepcionUsuario)
        {
          $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe ese estudio'}})";
            return $salida;
          }
        }
			  $salida = "({success: true, mensaje:'El estudio fue creado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'No se ha autenticado'}})";
		  }
    		


    		  
	  return $salida;
	  
    }
	

	
	protected function  updateArticulo()
    {
		$idArticulo = $this->getRequestParameter('id');
		
	    $criterio   =   new Criteria();
		$criterio->add(ArticuloPeer::ID_ARTICULO, $idArticulo);
	
		$articulo  = ArticuloPeer::doSelectOne($criterio);
		$salida	='';
		
		if($articulo)
		{
		
		
	        //DATOS ARTICULO
	        $titulo=$this->getRequestParameter('titulo');
	        $anno=$this->getRequestParameter('anno');
            $resumen=$this->getRequestParameter('articulo');

            
            if ($titulo) $articulo->setTitulo($titulo);
            if ($anno) $articulo->setAnno($anno);
            if ($resumen) $articulo->setArticulo($resumen);
      
            
        try
        {
          $articulo->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un estudio con la misma id'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'El Articulo fue actualizado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El estudio no existe'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteArticulo()
    {
        $idArticulos = json_decode($this->getRequestParameter('ids'));
        
        //	echo '"cc: '.$idUsuarios[0].'"';
        //	print_r($idPersonas);
        //   return sfView::NONE;		
        $criteria = new Criteria();
        $criteria->add(ArticuloPeer::ID_ARTICULO, $idArticulos, Criteria::IN);
        
        
        $estudios  = ArticuloPeer::doSelect($criteria);
        
        
        // return sfView::NONE;		
        
        //	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";
        
        
        $nombre="";
        $apellido="";	
        
        
        foreach ($estudios As $temporal)
        {
        
        //	print_r($usuario);
        
        
        
        
        
            $temporal->delete();
        
        
         
        
        } 
        
        if( count($idArticulos)<2)
        
        		  $salida = "({success: true, mensaje:'El Art&iacute;culo fue borrado exitosamente'})";
        	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
        else 
            $salida = "({success: true, mensaje:'Los articulos fueron borrados exitosamente'})";
                 
          
        
        return $salida;


	} 
      
}
