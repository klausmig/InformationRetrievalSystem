<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class capacitacionActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarCapacitaciones();
				break;

			case "UPDATEEST":
				$salida = $this->updateCapacitacion();
				break;
			case "CREATEEST":
				$salida = $this->crearCapacitacion();
				break;
			case "DELETEEST":
				$salida = $this->deleteCapacitacion();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarCapacitaciones()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
	      
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
        //$idPersona=6;
	    
                
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		
		$conexion->add(CapacitacionPeer::PERSONA_ID, $idPersona);

		$estudios = CapacitacionPeer::doSelect($conexion);
		
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($estudios As $estudio)
		{

          

          
            //DATOS ESTUDIOS
            $datos[$pos]['id']=$estudio->getIdCapacitacion();
            $datos[$pos]['numero']=$estudio->getNumero();
            $datos[$pos]['institucion']=$estudio->getInstitucion();
            $datos[$pos]['anno']=$estudio->getAnno();
            $datos[$pos]['capacitacion']=$estudio->getCapacitacion();
		   
          
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}





	protected function  crearCapacitacion()
    {
        $salida	='';

       		
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
     //   $idPersona=6;
	    

		$salida	='';
	
        		
		if($idPersona)
		{
		
		    $conexion = new Criteria();
            $conexion->add(CapacitacionPeer::PERSONA_ID, $idPersona);
            $conexion->addDescendingOrderByColumn(CapacitacionPeer::NUMERO);
            $conexion->setLimit(1);
            
            $articulo = CapacitacionPeer::doSelectOne($conexion);	
            
            if($articulo)
    		        $numero=$articulo->getNumero();
            else $numero=0;
            
            $numero++;			
		
            $estudio = new Capacitacion();
            
            
            $estudio->setPersonaId($idPersona);
            
            $estudio->setNumero($numero);
              			  
            $estudio->setInstitucion($this->getRequestParameter('institucion'));

            $estudio->setAnno($this->getRequestParameter('anno'));
            

            $estudio->setCapacitacion($this->getRequestParameter('capacitacion'));
			 
			  

              
              
        try
        {
          $estudio->save();
        }
        catch (Exception $excepcionUsuario)
        {
          $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe esa capacitaci&oacute;n'}})";
            return $salida;
          }
        }
			  $salida = "({success: true, mensaje:'La capacitaci&oacute;n fue creada exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'No se ha autenticado'}})";
		  }
    		


    		  
	  return $salida;
	  
    }
	

	
	protected function  updateCapacitacion()
    {
		$idCapacitacion = $this->getRequestParameter('id');
		
	    $criterio   =   new Criteria();
		$criterio->add(CapacitacionPeer::ID_CAPACITACION, $idCapacitacion);
	
		$estudio  = CapacitacionPeer::doSelectOne($criterio);
		$salida	='';
		
		if($estudio)
		{
		
		
	        //DATOS PERFIL
	        $institucion=$this->getRequestParameter('institucion');
	        $anno=$this->getRequestParameter('anno');
            $capacitacion=$this->getRequestParameter('capacitacion');

            
            if ($institucion) $estudio->setInstitucion($institucion);
            if ($anno) $estudio->setAnno($anno);
            if ($capacitacion) $estudio->setCapacitacion($capacitacion);
      
            
        try
        {
          $estudio->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un estudio con la misma id'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'La Capacitaci&oacute;n fue actualizada exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El estudio no existe'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteCapacitacion()
    {
        $idCapacitacions = json_decode($this->getRequestParameter('ids'));
        
        //	echo '"cc: '.$idUsuarios[0].'"';
        //	print_r($idPersonas);
        //   return sfView::NONE;		
        $criteria = new Criteria();
        $criteria->add(CapacitacionPeer::ID_CAPACITACION, $idCapacitacions, Criteria::IN);
        
        
        $estudios  = CapacitacionPeer::doSelect($criteria);
        
        
        // return sfView::NONE;		
        
        //	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";
        
        
        $nombre="";
        $apellido="";	
        
        
        foreach ($estudios As $temporal)
        {
        
        //	print_r($usuario);
        
        
        
        
        
            $temporal->delete();
        
        
         
        
        } 
        
        if( count($idCapacitacions)<2)
        
        		  $salida = "({success: true, mensaje:'La Capacitaci&oacute;n fue borrada exitosamente'})";
        	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
        else 
            $salida = "({success: true, mensaje:'Las Capacitaci&oacute;nes fueron borradas exitosamente'})";
                 
          
        
        return $salida;


	} 
      
}
