<?php

/**
 * upload actions.
 *
 * @package    ingreso
 * @subpackage upload
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class uploadActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
   // $this->forward('default', 'module');
  }
  
  
  public function executeUser()
  {

  	$cedula=$this->getUser()->getAttribute('cedula');

  	 return $this->renderText("{success: true,user: {cedula:'".$cedula."'}}");


	//return $this->renderText("{success: false, errors: { reason: 'Login y/o contrase&ntilde;a incorrectos. Por favor intentelo nuevamente.' }}");

  	// return $this->renderText("{success: true,user: {cedula:'".$cedula."'}}");
  }
}
