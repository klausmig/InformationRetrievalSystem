<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class estudioActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarEstudios();
				break;

			case "UPDATEEST":
				$salida = $this->updateEstudio();
				break;
			case "CREATEEST":
				$salida = $this->crearEstudio();
				break;
			case "DELETEEST":
				$salida = $this->deleteEstudio();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarEstudios()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
	      
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
        //$idPersona=6;
	    
                
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		
		$conexion->add(EstudioPeer::PERSONA_ID, $idPersona);

		$estudios = EstudioPeer::doSelect($conexion);
		
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($estudios As $estudio)
		{

          

          
            //DATOS ESTUDIOS
            $datos[$pos]['id']=$estudio->getIdEstudio();
            $datos[$pos]['numero']=$estudio->getNumero();
            $datos[$pos]['institucion']=$estudio->getInstitucion();
            $datos[$pos]['lugar']=$estudio->getLugar();
            $datos[$pos]['anno']=$estudio->getAnno();
            
            $datos[$pos]['tipo_estudio']=$estudio->getTipoEstudio();
            $datos[$pos]['titulo']=$estudio->getTitulo();
		   
          
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}





	protected function  crearEstudio()
    {
        $salida	='';

       		
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
     //   $idPersona=6;
	    

		$salida	='';
	
        		
		if($idPersona)
		{
		
		    $conexion = new Criteria();
            $conexion->add(EstudioPeer::PERSONA_ID, $idPersona);
            $conexion->addDescendingOrderByColumn(EstudioPeer::NUMERO);
            $conexion->setLimit(1);
            
            $articulo = EstudioPeer::doSelectOne($conexion);	
            
            if($articulo)
    		        $numero=$articulo->getNumero();
            else $numero=0;
            
            $numero++;		
		
            $estudio = new Estudio();
            
            
            $estudio->setPersonaId($idPersona);
            $estudio->setNumero($numero);
              			  
            $estudio->setInstitucion($this->getRequestParameter('institucion'));
            $estudio->setLugar($this->getRequestParameter('lugar'));
            $estudio->setAnno($this->getRequestParameter('anno'));
            
            $estudio->setTipoEstudio($this->getRequestParameter('tipo_estudio'));
            $estudio->setTitulo($this->getRequestParameter('titulo'));
			 
			  

              
              
        try
        {
          $estudio->save();
        }
        catch (Exception $excepcionUsuario)
        {
          $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe ese estudio'}})";
            return $salida;
          }
        }
			  $salida = "({success: true, mensaje:'El estudio fue creado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'No se ha autenticado'}})";
		  }
    		


    		  
	  return $salida;
	  
    }
	

	
	protected function  updateEstudio()
    {
		$idEstudio = $this->getRequestParameter('id');
		
	    $criterio   =   new Criteria();
		$criterio->add(EstudioPeer::ID_ESTUDIO, $idEstudio);
	
		$estudio  = EstudioPeer::doSelectOne($criterio);
		$salida	='';
		
		if($estudio)
		{
		
		
	        //DATOS PERFIL
	        $institucion=$this->getRequestParameter('institucion');
	        $lugar=$this->getRequestParameter('lugar');
	        $anno=$this->getRequestParameter('anno');
	        $tipo_estudio=$this->getRequestParameter('tipo_estudio');
            $titulo=$this->getRequestParameter('titulo');

            
            if ($institucion) $estudio->setInstitucion($institucion);
            if ($lugar) $estudio->setLugar($lugar);		
            if ($anno) $estudio->setAnno($anno);
            if ($tipo_estudio) $estudio->setTipoEstudio($tipo_estudio);
            if ($titulo) $estudio->setTitulo($titulo);
      
            
        try
        {
          $estudio->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un estudio con la misma id'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'El Estudio fue actualizado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El estudio no existe'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteEstudio()
    {
        $idEstudios = json_decode($this->getRequestParameter('ids'));
        
        //	echo '"cc: '.$idUsuarios[0].'"';
        //	print_r($idPersonas);
        //   return sfView::NONE;		
        $criteria = new Criteria();
        $criteria->add(EstudioPeer::ID_ESTUDIO, $idEstudios, Criteria::IN);
        
        
        $estudios  = EstudioPeer::doSelect($criteria);
        
        
        // return sfView::NONE;		
        
        //	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";
        
        
        $nombre="";
        $apellido="";	
        
        
        foreach ($estudios As $temporal)
        {
        
        //	print_r($usuario);
        
        
        
        
        
            $temporal->delete();
        
        
         
        
        } 
        
        if( count($idEstudios)<2)
        
        		  $salida = "({success: true, mensaje:'El estudio fue borrado exitosamente'})";
        	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
        else 
            $salida = "({success: true, mensaje:'Los estudios fueron borrados exitosamente'})";
                 
          
        
        return $salida;


	} 
      
}
