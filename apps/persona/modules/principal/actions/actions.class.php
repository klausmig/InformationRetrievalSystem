<?php

/**
 * principal actions.
 *
 * @package    ingreso
 * @subpackage principal
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class principalActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
  
  public function executeUser()
  {
  
  	$name=$this->getUser()->getAttribute('nombre');
  	return $this->renderText("{success: true,user: {name:'".$name."'}}");
  
  }
}
