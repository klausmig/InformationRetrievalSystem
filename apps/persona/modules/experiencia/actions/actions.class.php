<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class experienciaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarExperiencias();
				break;

			case "UPDATEEST":
				$salida = $this->updateExperiencia();
				break;
			case "CREATEEST":
				$salida = $this->crearExperiencia();
				break;
			case "DELETEEST":
				$salida = $this->deleteExperiencia();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarExperiencias()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
	      
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
        //$idPersona=6;
	    
                
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		
		$conexion->add(ExperienciaPeer::PERSONA_ID, $idPersona);

		$experiencias = ExperienciaPeer::doSelect($conexion);
		
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($experiencias As $experiencia)
		{

          

          
            //DATOS EXPERIENCIAS
            $datos[$pos]['id']=$experiencia->getIdExperiencia();
            $datos[$pos]['numero']=$experiencia->getNumero();
            $datos[$pos]['institucion']=$experiencia->getInstitucion();
            $datos[$pos]['fecha_inicio']=$experiencia->getFechaInicio();
            $datos[$pos]['fecha_fin']=$experiencia->getFechaFin();
            
            $datos[$pos]['logro']=$experiencia->getLogro();
            $datos[$pos]['responsabilidad']=$experiencia->getResponsabilidad();
		   
          
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}





	protected function  crearExperiencia()
    {
        $salida	='';

       		
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
     //   $idPersona=6;
	    

		$salida	='';
	
        		
		if($idPersona)
		{
		
		    $conexion = new Criteria();
            $conexion->add(ExperienciaPeer::PERSONA_ID, $idPersona);
            $conexion->addDescendingOrderByColumn(ExperienciaPeer::NUMERO);
            $conexion->setLimit(1);
            
            $articulo = ExperienciaPeer::doSelectOne($conexion);	
            
            if($articulo)
    		        $numero=$articulo->getNumero();
            else $numero=0;
            
            $numero++;			
		
            $experiencia = new Experiencia();
            
            
            $experiencia->setPersonaId($idPersona);
            
            $experiencia->setNumero($numero);
              			  
            $experiencia->setInstitucion($this->getRequestParameter('institucion'));
            $experiencia->setfechaInicio($this->getRequestParameter('fecha_inicio'));
            $experiencia->setFechaFin($this->getRequestParameter('fecha_fin'));
            
            $experiencia->setLogro($this->getRequestParameter('logro'));
            $experiencia->setResponsabilidad($this->getRequestParameter('responsabilidad'));
			 
			  

              
              
        try
        {
          $experiencia->save();
        }
        catch (Exception $excepcionUsuario)
        {
          $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe ese experiencia'}})";
            return $salida;
          }
        }
			  $salida = "({success: true, mensaje:'La experiencia fue creada exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'No se ha autenticado'}})";
		  }
    		


    		  
	  return $salida;
	  
    }
	

	
	protected function  updateExperiencia()
    {
		$idExperiencia = $this->getRequestParameter('id');
		
	    $criterio   =   new Criteria();
		$criterio->add(ExperienciaPeer::ID_EXPERIENCIA, $idExperiencia);
	
		$experiencia  = ExperienciaPeer::doSelectOne($criterio);
		$salida	='';
		
		if($experiencia)
		{
		
		
	        //DATOS PERFIL
	        $institucion=$this->getRequestParameter('institucion');
	        $fechaInicio=$this->getRequestParameter('fecha_inicio');
	        $fechaFin=$this->getRequestParameter('fecha_fin');
	        $logro=$this->getRequestParameter('logro');
            $responsabilidad=$this->getRequestParameter('responsabilidad');

            
            if ($institucion) $experiencia->setInstitucion($institucion);
            if ($fechaInicio) $experiencia->setFechaInicio($fechaInicio);		
            if ($fechaFin) $experiencia->setFechaFin($fechaFin);
            if ($logro) $experiencia->setLogro($logro);
            if ($responsabilidad) $experiencia->setResponsabilidad($responsabilidad);
      
            
        try
        {
          $experiencia->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un experiencia con la misma id'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'La experiencia fue actualizada exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'La experiencia laboral no existe'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteExperiencia()
    {
        $idExperiencias = json_decode($this->getRequestParameter('ids'));
        
        //	echo '"cc: '.$idUsuarios[0].'"';
        //	print_r($idPersonas);
        //   return sfView::NONE;		
        $criteria = new Criteria();
        $criteria->add(ExperienciaPeer::ID_EXPERIENCIA, $idExperiencias, Criteria::IN);
        
        
        $experiencias  = ExperienciaPeer::doSelect($criteria);
        
        
        // return sfView::NONE;		
        
        //	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";
        
        
        $nombre="";
        $apellido="";	
        
        
        foreach ($experiencias As $temporal)
        {
        
        //	print_r($usuario);
        
        
        
        
        
            $temporal->delete();
        
        
         
        
        } 
        
        if( count($idExperiencias)<2)
        
        		  $salida = "({success: true, mensaje:'La experiencia fue borrada exitosamente'})";
        	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
        else 
            $salida = "({success: true, mensaje:'Las experiencias laborales fueron borradas exitosamente'})";
                 
          
        
        return $salida;


	} 
      
}
