<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class idiomaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarIdiomaes();
				break;

			case "UPDATEEST":
				$salida = $this->updateIdioma();
				break;
			case "CREATEEST":
				$salida = $this->crearIdioma();
				break;
			case "DELETEEST":
				$salida = $this->deleteIdioma();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarIdiomaes()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
	      
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
        //$idPersona=6;
	    
                
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		
		$conexion->add(IdiomaPeer::PERSONA_ID, $idPersona);
		$conexion->addAscendingOrderByColumn(IdiomaPeer::IDIOMA);

		$idiomas = IdiomaPeer::doSelect($conexion);
		
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($idiomas As $idioma)
		{

          

          
            //DATOS ESTUDIOS
            $datos[$pos]['id']=$idioma->getIdIdioma();
            $datos[$pos]['idioma']=$idioma->getIdioma();
            $datos[$pos]['habla']=$idioma->getHabla();
            $datos[$pos]['escribe']=$idioma->getEscribe();
            $datos[$pos]['lee']=$idioma->getLee();
		   
          
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}



		


	protected function  crearIdioma()
    {
        $salida	='';

       		
		$idPersona=$this->getUser()->getAttribute('idPersona');
		
     //   $idPersona=6;
	    

		$salida	='';
	
        		
		if($idPersona)
		{
		
		
		
		    $conexion = new Criteria();
            $conexion->add(IdiomaPeer::PERSONA_ID, $idPersona);
            $conexion->add(IdiomaPeer::IDIOMA,$this->getRequestParameter('idioma'));
            $conexion->addDescendingOrderByColumn(IdiomaPeer::IDIOMA);
            $conexion->setLimit(1);
            
            $idioma = IdiomaPeer::doSelectOne($conexion);	
            
            if(!($idioma))
            {
 
            
          
	    
		
                $idioma = new Idioma();
                
                
                $idioma->setPersonaId($idPersona);
                
                $idioma->setIdioma($this->getRequestParameter('idioma'));
                		  
                $idioma->setHabla($this->getRequestParameter('habla'));
                
                $idioma->setEscribe($this->getRequestParameter('escribe'));
                
                
                $idioma->setLee($this->getRequestParameter('lee'));
                
                
                
                
                
                try
                {
                $idioma->save();
                }
                catch (Exception $excepcionUsuario)
                {
                    $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
                    if($exeptionErrorUsuario > 0)
                    {
                        $salida = "({success: false, errors: { reason: 'Ya existe este Idioma'}})";
                        return $salida;
                    }
                }
                $salida = "({success: true, mensaje:'El idioma fue creado exitosamente'})";
		  
          }
          else
          {
          
            $salida = "({success: false, errors: { reason: 'Ya existe este Idioma'}})";
            return $salida;
          
          }
          
        } 
        else 
        {
		  $salida = "({success: false, errors: { reason: 'No se ha autenticado'}})";
		}
    		


    		  
	  return $salida;
	  
    }
	

	
	protected function  updateIdioma()
    {
		$idIdioma = $this->getRequestParameter('id');
		
	    $criterio   =   new Criteria();
		$criterio->add(IdiomaPeer::ID_IDIOMA, $idIdioma);
	
		$idioma  = IdiomaPeer::doSelectOne($criterio);
		$salida	='';
		
		if($idioma)
		{
		
		
	        //DATOS IDIOMA
	        $nombre=$this->getRequestParameter('idioma');
	        $habla=$this->getRequestParameter('habla');
            $escribe=$this->getRequestParameter('escribe');
            $lee=$this->getRequestParameter('lee');

            
            if ($nombre) $idioma->setIdioma(strtoupper($nombre));
            if ($habla) $idioma->setHabla($habla);
            if ($escribe) $idioma->setEscribe($escribe);
            if ($lee) $idioma->setLee($lee);
      
            
        try
        {
          $idioma->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un idioma con la misma id'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'El Idioma fue actualizado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El idioma no existe'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteIdioma()
    {
        $idIdiomas = json_decode($this->getRequestParameter('ids'));
        
        //	echo '"cc: '.$idUsuarios[0].'"';
        //	print_r($idPersonas);
        //   return sfView::NONE;		
        $criteria = new Criteria();
        $criteria->add(IdiomaPeer::ID_IDIOMA, $idIdiomas, Criteria::IN);
        
        
        $idiomas  = IdiomaPeer::doSelect($criteria);
        
        
        // return sfView::NONE;		
        
        //	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";
        
        
        $nombre="";
        $apellido="";	
        
        
        foreach ($idiomas As $temporal)
        {
        
        //	print_r($usuario);
        
        
        
        
        
            $temporal->delete();
        
        
         
        
        } 
        
        if( count($idIdiomas)<2)
        
        		  $salida = "({success: true, mensaje:'El Art&iacute;culo fue borrado exitosamente'})";
        	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
        else 
            $salida = "({success: true, mensaje:'Los idiomas fueron borrados exitosamente'})";
                 
          
        
        return $salida;


	} 
      
}
