<?php

/**
 * autenticacion actions.
 *
 * @package    ingreso
 * @subpackage autenticacion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class autenticacionActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
   // $this->forward('default', 'module');
  }
  
  
  
  	public function executeIniciarSesion()
	{
		$c = new Criteria();
		$c->add(UsuarioPeer::LOGIN, $this->getRequestParameter('usuario'));
		$c->addJoin(PersonaPeer::USUARIO_ID,UsuarioPeer::ID_USUARIO);
		$persona = PersonaPeer::doSelectOne($c);
		
		if($persona)
		{
		
		    $usuario=$persona->getUsuario();
		    
			if($usuario->getEstado() == 1)
			{
			
			$clave_decode= base64_decode($usuario->getClave());
			//echo "<br><pre>".$clave_decode;
		//	echo "<br><pre>".$usuario->getContrasena()." ".md5($this->getRequestParameter('contrasena'));
				if($clave_decode ==  $this->getRequestParameter('contrasena'))
        //md5($this->getRequestParameter('contrasena')))
				{
					$this->getUser()->setAttribute('idUsuario', $usuario->getIdUsuario());
					$this->getUser()->setAttribute('login', $this->getRequestParameter('usuario'));
					$this->getUser()->setAttribute('idPersona', $persona->getIdPersona());
					$this->getUser()->setAttribute('cedula', $persona->getCedula());
					$this->getUser()->setAttribute('nombre', $persona->getNombre()." ".$persona->getApellido());

					$this->getUser()->setAttribute('contrasena', $usuario->getClave());
					$this->getUser()->setAttribute('idUsuAutenticado', $usuario->getIdUsuario());

					$this->getUser()->setAttribute('estado', $usuario->getEstado());
					$this->getUser()->setAttribute('tipo_usuario', $usuario->getTipoUsuario());
			//		$this->getUser()->setAttribute('edad_perfil', $usuario->getEdad());
					$this->getUser()->setAuthenticated(true);
					
					$this->getUser()->clearCredentials();
					$this->getUser()->addCredential($usuario->getTipoUsuario());
					return $this->renderText("{success: true,perfil: {value:'".$usuario->getTipoUsuario()."'}}");
				}
				else
					return $this->renderText("{success: false, errors: { reason: 'Login y/o contrase&ntilde;a incorrectos. Por favor intentelo nuevamente.' }}");
			}
			else
				return $this->renderText("{success: false, errors: { reason: 'Usted no se encuentra habilitado para entrar al sistema, Pongase en contacto con el administrador.' }}");
		}
		else
			return $this->renderText("{success: false, errors: { reason: 'Login y/o contrase&ntilde;a incorrectos. Por favor intentelo nuevamente.' }}");
	}

	/**
	* Executes cerrarSesion action
	*
	*/
	public function executeCerrarSesion()
	{
		$this->getUser()->getAttributeHolder()->clear();
		$this->getUser()->setAuthenticated(false);
		$this->getUser()->clearCredentials();
		$this->redirect('index.php/autenticacion/');
	}
	
    public function executeImagen()
	{
		      // Mostrar el formulario*/
			function generateCode() {
			/* list all possible characters, similar looking characters and vowels have been removed */
			$possible = '23456789bcdfghjkmnpqrstvwxyz';
			$code = '';
			$i = 0;
			while ($i < 5) { 
				$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
				$i++;
			}
			return $code;
			}
		      $strindCaptcha = generateCode();
	// 	      $this->getUser()->setAttribute('captcha', $strindCaptcha);
		      return $this->renderText($strindCaptcha);
	//               return sfView::NONE;
	}
/*	
	public function executeRegistro()
	{
	      $textoCaptchaServer = $this->getUser()->getAttribute('captcha');

	      //Tomar un dato del formulario
	      $loginF     = $this->getRequestParameter('login');
	      $nombreF    = $this->getRequestParameter('nombre');
	      $apellidosF = $this->getRequestParameter('apellidos');
	      $profesionF = $this->getRequestParameter('profesion');
	      $correoF    = $this->getRequestParameter('correo');
	      $paisF      = $this->getRequestParameter('pais');
	      $contrasenaF = $this->getRequestParameter('contrasena');
	      $textoCaptcha = $this->getRequestParameter('areaTextCaptcha');
	      $contEncrip = md5($contrasenaF);
	      /*tomar un dato de la base de datos
	      $criterioF = new Criteria();
	      $criterioF->setIgnoreCase(true);
	      $criterioF->add(UsuarioPeer::LOGIN, $loginF);
	      $loginF    = UsuarioPeer::doSelectOne($criterioF);
	      $this->logueoExiste  = "false";
	      $this->captchaMalo = false;
        if ($loginF || $textoCaptcha != $textoCaptchaServer)
        { //verifica si el login existe en la bd
            if ($textoCaptcha != $textoCaptchaServer)
            {
              $this->captchaMalo = "true";
            }
            else
            {
              $this->logueoExiste  = "true";
            }//end if
        }
        else
        {
            //guardar los datos en la bd
            $loginF     = $this->getRequestParameter('login');
            $usuarioF = new Usuario();
            $usuarioF->setLogin($loginF);
            $usuarioF->setNombre($nombreF);
            $usuarioF->setApellidos($apellidosF);
            $usuarioF->setProfesion($profesionF);
            $usuarioF->setCorreo($correoF);
            $usuarioF->setPais($paisF);
            $usuarioF->setContrasena($contEncrip);
            $usuarioF->save();
            return $this->redirect('login/login');
            
            
	      }//end else del if verifica si el login existe en la bd
	}
*/	
	public function executeCreate()
	{
		$idUsuarios = $this->getRequestParameter('cedula');
		$criterio   =   new Criteria();
		$criterio->add(PersonaPeer::CEDULA, $idUsuarios);
		$persona = PersonaPeer::doSelectOne($criterio);
		$salida	='';
		
		if(!$persona)
		{
			  $usuario = new Usuario();
			  
              $usuario->setLogin($this->getRequestParameter('login'));
			  $usuario->setClave(base64_encode($this->getRequestParameter('clave')));
			  $usuario->setTipoUsuario($this->getRequestParameter('tipo_usuario'));
			  $usuario->setEstado($this->getRequestParameter('estado'));
			  
			  $persona = new Persona();
			  
			  $persona->setNombre($this->getRequestParameter('nombre'));
			  $persona->setApellido($this->getRequestParameter('apellido'));
			  $persona->setSexo($this->getRequestParameter('sexo'));
			  $persona->setCedula($this->getRequestParameter('cedula'));
			  
			  $persona->setEmail($this->getRequestParameter('email'));
			  $persona->setTelefonoFijo($this->getRequestParameter('telefono'));
			  $persona->setTelefonoCelular($this->getRequestParameter('celular'));
			  $persona->setTipoDoc($this->getRequestParameter('tipo_identificacion'));
			  $persona->setUsuario($usuario);
			  

			 // $usuario->setEdad($this->getRequestParameter('Edad'));
			$perfil = new Perfil();
			
			$perfil->setPersona($persona);
              
              
        try
        {
          $perfil->save();
        }
          
        catch (Exception $excepcionUsuario)
        {
          $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe un usuario con el nombre de usuario igual a: ".$this->getRequestParameter('Login')."'}})";
            return $salida;
          }
        }
			  $salida = "({success: true, mensaje:'El usuario fue creado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'Ya existe un usuario con ese mismo No. de c�dula'}})";
		  }
		  return $this->renderText($salida);
	}	
}
