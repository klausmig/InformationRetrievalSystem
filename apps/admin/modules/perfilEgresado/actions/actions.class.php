<?php

/**
 * perfilEgresado actions.
 *
 * @package    ingreso
 * @subpackage perfilEgresado
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class perfilEgresadoActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarUsuarios();
				break;

			case "LISTINGPERFIL":
				$salida = $this->listarPerfil();
				break;

			case "UPDATEUSU":
				$salida = $this->updateUsuario();
				break;
			case "CREATEUSU":
				$salida = $this->crearUsuario();
				break;
			case "DELETEUSU":
				$salida = $this->deleteUsuario();
				break;
			default:
				$salida =  "({failure:true})";
				break;
		}

		

		return $this->renderText($salida);
	}

	protected function listarUsuarios()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
	      // $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
	      
		if ($this->getRequestParameter('estado_perfil')!=null )
		{
         $conexion->add(PerfilPeer::ESTADO_PERFIL,$this->getRequestParameter('estado_perfil'));
        
        }
		//	      
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		$conexion->add(UsuarioPeer::TIPO_USUARIO, 'Egresado');
		$conexion->addJoin(PersonaPeer::USUARIO_ID,UsuarioPeer::ID_USUARIO);
		$conexion->addJoin(PerfilPeer::PERSONA_ID,PersonaPeer::ID_PERSONA);
		$perfiles = PerfilPeer::doSelect($conexion);
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($perfiles As $perfil)
		{
		   // $filtro = new Criteria();
          //  $filtro->add(PersonaPeer::USUARIO_ID, $temporal->getIdUsuario());
          
            $persona= $perfil->getPersona();
          
            $usuario = $persona->getUsuario();
          
		 //   $info_usuario = PersonaPeer::doSelect($filtro);
		  //  $info =  $info_usuario[0];
		    
		   // echo "<br><pre>".$usuario;
		   
		   //DATOS USUARIO
            $datos[$pos]['id']=$usuario->getIdUsuario();
            $datos[$pos]['login']=$usuario->getLogin();
            $datos[$pos]['clave']=base64_decode( $usuario->getClave());
            $datos[$pos]['tipo_usuario']=$usuario->getTipoUsuario();
            $datos[$pos]['estado']=$usuario->getEstado();
		   
		   //DATOS PERSONA
            $datos[$pos]['cedula']=$persona->getCedula();
            $datos[$pos]['tipo_cedula']=$persona->getTipoDoc();		
            $datos[$pos]['nombre']=$persona->getNombre();
            $datos[$pos]['apellido']=$persona->getApellido();
            $datos[$pos]['sexo']=$persona->getSexo();
            $datos[$pos]['telefono']=$persona->getTelefonoFijo();
            $datos[$pos]['celular']=$persona->getTelefonoCelular();
            $datos[$pos]['email']=$persona->getEmail();
            $datos[$pos]['direccion']=$persona->getDireccion();
            $datos[$pos]['ciudad']=$persona->getCiudad();
            $datos[$pos]['fecha_nacimiento']=$persona->getFechaNacimiento();
            $datos[$pos]['departamento']=$persona->getDepartamento();
            
            //DATOS PERFIL
            
            $datos[$pos]['nivel_academico']=$perfil->getNivelAcademico();
            $datos[$pos]['area_estudio']=$perfil->getAreaEstudio();		
            $datos[$pos]['estado_estudio']=$perfil->getEstadoEstudio();
            $datos[$pos]['estado_perfil']=$perfil->getEstadoPerfil();
            $datos[$pos]['idiomas']=$perfil->getIdiomas();
            $datos[$pos]['becas']=$perfil->getBecas();
            $datos[$pos]['resumen']=$perfil->getResumen();          
            
            //$datos[$pos]['usu_per_nombre']=$this->getTipoUusario($temporal->getUsuPerId());
            //	$datos[$pos]['usu_per_id']=$temporal->getUsuPerId();
            
           
            
            
            
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}
/*
	protected function listarPerfil()
	{


		$conexion = new Criteria();
		$numero_Perfil = SipexPerfilPeer::doCount($conexion);
	        $perfil = SipexPerfilPeer::doSelect($conexion);
		$pos = 0;
		$nbrows=0;
		$datos;

		foreach ($perfil As $temporal)
		{
			$datos[$pos]['perId']=$temporal->getPerId();
			$datos[$pos]['perNombre']=$temporal->getPerNombre();
			
			$pos++;
			$nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}



	protected function getNombrePerfil($idPerfil)
	{

		$conexion = new Criteria();
		$numero_Perfil = SipexPerfilPeer::doCount($conexion);
	        $conexion->add(SipexPerfilPeer::PER_ID,$idPerfil); //esto es un where
		$perfil = SipexPerfilPeer::doSelect($conexion);
		$pos = 0;
		$nbrows=0;

		foreach ($perfil As $temporal)
		{
			$nombrePerfil=$temporal->getPerNombre();
			$pos++;
			$nbrows++;
		}

		return $nombrePerfil;
	}


	protected function  crearUsuario()
    {
        $salida	='';

       		
		$idPersona = $this->getRequestParameter('cedula');
	    $criterio   =   new Criteria();
		$criterio->add(PersonaPeer::CEDULA, $idPersona);
		$persona  = PersonaPeer::doSelectOne($criterio);
		$salida	='';
	
        		
		if(!$persona)
		{
			  $usuario = new Usuario();
			      			  
			  $usuario->setLogin($this->getRequestParameter('login'));
			  $usuario->setClave(base64_encode ($this->getRequestParameter('clave')));
			  $usuario->setTipoUsuario($this->getRequestParameter('tipo_usuario'));
			  $usuario->setEstado($this->getRequestParameter('estado'));
			  
              $persona = new Persona();
               
              $persona->setCedula($this->getRequestParameter('cedula'));
			  $persona->setTipoDoc($this->getRequestParameter('tipo_identificacion'));
			  $persona->setNombre($this->getRequestParameter('nombre'));
			  $persona->setApellido($this->getRequestParameter('apellido'));
			  $persona->setSexo($this->getRequestParameter('sexo'));
			  $persona->setTelefonoFijo($this->getRequestParameter('telefono'));
			  $persona->setEmail($this->getRequestParameter('email'));
			  $persona->setUsuario($usuario);
              
              
        try
        {
          $persona->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
          if($exeptionErrorUsuario > 0)
          {
            $salida = "({success: false, errors: { reason: 'Ya existe una cuenta con el mismo nombre de usuario'}})";
            return $salida;
         // }
        }
			  $salida = "({success: true, mensaje:'El usuario fue creado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'Ya existe un usuario con ese mismo No. de c�dula'}})";
		  }
    		


    		  
	  return $salida;
	  
    }
	
*/
	
	protected function  updateUsuario()
    {
		$idPersona = $this->getRequestParameter('cedula');
	    $criterio   =   new Criteria();
		$criterio->add(PersonaPeer::CEDULA, $idPersona);
		$criterio->addJoin(PerfilPeer::PERSONA_ID,PersonaPeer::ID_PERSONA);
		$perfil  = PerfilPeer::doSelectOne($criterio);
		$salida	='';
		if($perfil)
		{
		
		
	        //DATOS PERFIL
	        $nivel=$this->getRequestParameter('nivel_academico');
	        $area=$this->getRequestParameter('area_estudio');
	        $estado_estudio=$this->getRequestParameter('estado_estudio');
	        $estado_perfil=$this->getRequestParameter('estado_perfil');
	        $idiomas=$this->getRequestParameter('idiomas');
	        $becas=$this->getRequestParameter('becas');
	        $resumen=$this->getRequestParameter('resumen');
            
            if ($nivel!=null) $perfil->setNivelAcademico($nivel);
            if ($area!=null) $perfil->setAreaEstudio($area);		
            if ($estado_estudio!=null) $perfil->setEstadoEstudio($estado_estudio);
            if ($estado_perfil!=null) $perfil->setEstadoPerfil($estado_perfil);
            if ($idiomas!=null) $perfil->setIdiomas($idiomas);
            if ($becas!=null) $perfil->setBecas($becas);
            if ($resumen!=null) $perfil->setResumen($resumen);  
            	  
            $persona=$perfil->getPersona();
            
            $persona->setTipoDoc($this->getRequestParameter('tipo_cedula'));
            $persona->setNombre($this->getRequestParameter('nombre'));
            $persona->setApellido($this->getRequestParameter('apellido'));
            $persona->setSexo($this->getRequestParameter('pef_sexo'));
            $persona->setTelefonoFijo($this->getRequestParameter('telefono'));
            $persona->setTelefonoCelular($this->getRequestParameter('celular'));
            $persona->setEmail($this->getRequestParameter('email'));
            $persona->setDireccion($this->getRequestParameter('direccion'));
            $persona->setFechaNacimiento($this->getRequestParameter('fecha_nacimiento'));
            $persona->setCiudad($this->getRequestParameter('ciudad'));
            $persona->setDepartamento($this->getRequestParameter('departamento'));
            
        /*    $usuario=$persona->getUsuario();
            
            $usuario->setLogin($this->getRequestParameter('login'));
            $usuario->setClave(base64_encode ($this->getRequestParameter('clave')));
            $usuario->setTipoUsuario($this->getRequestParameter('tipo_usuario'));
            $usuario->setEstado($this->getRequestParameter('estado'));*/
            
            
        try
        {
          $perfil->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un usuario con el nombre de usuario igual a: ".$this->getRequestParameter('Login')."'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'El usuario fue actualizado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El usuario no existe con ese No. de c�dula'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteUsuario()
    {
		$idPersonas = json_decode($this->getRequestParameter('ids'));
		
			//	echo '"cc: '.$idUsuarios[0].'"';
		//	print_r($idPersonas);
  //   return sfView::NONE;		
			$criteria = new Criteria();
			$criteria->add(PersonaPeer::CEDULA, $idPersonas, Criteria::IN);
			
			
		   $personas  = PersonaPeer::doSelect($criteria);
		
				
    // return sfView::NONE;		

	//	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";

        
        $nombre="";
        $apellido="";	
        
	
    		foreach ($personas As $temporal)
    		{
    		
    	//	print_r($usuario);

                    
                  	$nombre=$temporal->getNombre();
                    $apellido=$temporal->getApellido();
                    $usuario=$temporal->getUsuario();
                    
                  
                    
                    $temporal->delete();
                    
                      $usuario->delete();
                    
                     $salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";       
                   
            } 
                if( count($idPersonas)<2)
                
                		  $salida = "({success: true, mensaje:'El usuario $nombre $apellido fue borrado exitosamente'})";
                	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
                 else 
                    $salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
                 
          
        
		  return $salida;


	} 
      
}
