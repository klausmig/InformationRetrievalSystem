<?php

/**
 * usuarioPersona actions.
 *
 * @package    ingreso
 * @subpackage usuarioPersona
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class usuarioPersonaActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
  
public function executeCargar()
	{
		$task = '';
		$salida	='';
// 		if ($this->getRequest()->getMethod() == sfRequest::POST)
// 		{
			$task = $this->getRequest()->getParameter('task');
// 		}
// 		echo '"'.$this->getRequest()->getParameter('task').'"';
// 		return sfView::NONE;
		switch($task){
			case "LISTING":
				$salida = $this->listarUsuarios();
				break;

			case "UPDATEUSU":
				$salida = $this->updateUsuario();
				break;
			case "CREATEUSU":
				$salida = $this->crearUsuario();
				break;
			case "DELETEUSU":
				$salida = $this->deleteUsuario();
				break;
			default:
				$salida =  "({failure:true})";
				break;
				
	/*		case "LISTINGPERFIL":
				$salida = $this->listarPerfil();
				break; */
                				
		}

		

		return $this->renderText($salida);
	}

	protected function listarUsuarios()
	{

		$conexion = new Criteria();
		$numero_Usuarios = UsuarioPeer::doCount($conexion);
		if ($this->getRequestParameter('estado')!=null )
		{
         $conexion->add(UsuarioPeer::ESTADO,$this->getRequestParameter('estado'));
        
        }
		// $conexion->add(SipexUsuarioPeer::USU_ESTADO,1); //esto es un where
		$conexion->setLimit($this->getRequestParameter('limit'));
		$conexion->setOffset($this->getRequestParameter('start'));
		$conexion->addJoin(PersonaPeer::USUARIO_ID,UsuarioPeer::ID_USUARIO);
		$personas = PersonaPeer::doSelect($conexion);
		$pos = 0;
		$nbrows=0;
		$datos;
		foreach ($personas As $temporal)
		{
		// $filtro = new Criteria();
		//  $filtro->add(PersonaPeer::USUARIO_ID, $temporal->getIdUsuario());

		$usuario = $temporal->getUsuario();

		//   $info_usuario = PersonaPeer::doSelect($filtro);
		//  $info =  $info_usuario[0];

		// echo "<br><pre>".$usuario;
		$datos[$pos]['id']=$usuario->getIdUsuario();
		$datos[$pos]['login']=$usuario->getLogin();
		$datos[$pos]['clave']=base64_decode( $usuario->getClave());
		$datos[$pos]['tipo_usuario']=$usuario->getTipoUsuario();
		$datos[$pos]['estado']=$usuario->getEstado();
		   
		   
		$datos[$pos]['cedula']=$temporal->getCedula();
		$datos[$pos]['tipo_identificacion']=$temporal->getTipoDoc();		
		$datos[$pos]['nombre']=$temporal->getNombre();
		$datos[$pos]['apellido']=$temporal->getApellido();
		$datos[$pos]['sexo']=$temporal->getSexo();
		$datos[$pos]['telefono']=$temporal->getTelefonoFijo();
		$datos[$pos]['celular']=$temporal->getTelefonoCelular();
		$datos[$pos]['email']=$temporal->getEmail();

            //$datos[$pos]['usu_per_nombre']=$this->getTipoUusario($temporal->getUsuPerId());
            //	$datos[$pos]['usu_per_id']=$temporal->getUsuPerId();
            
           
            
            
            
            $pos++;
            $nbrows++;
		}
		if($nbrows>0){
			$jsonresult = json_encode($datos);
			return '({"total":"'.$numero_Usuarios.'","results":'.$jsonresult.'})';
		}
		else {
			return '({"total":"0", "results":""})';
		}
	}




	protected function  crearUsuario()
    {
        $salida	='';

       		
		$idPersona = $this->getRequestParameter('cedula');
	    $criterio   =   new Criteria();
		$criterio->add(PersonaPeer::CEDULA, $idPersona);
		$persona  = PersonaPeer::doSelectOne($criterio);
		$salida	='';
	
        		
		if(!$persona)
		{
			$tipo_usuario = $this->getRequestParameter('tipo_usuario');
			$usuario = new Usuario();
			  
			$usuario->setLogin($this->getRequestParameter('login'));
			$usuario->setClave(base64_encode ($this->getRequestParameter('clave')));
			$usuario->setTipoUsuario($tipo_usuario);
			$usuario->setEstado($this->getRequestParameter('estado'));

			$persona = new Persona();

			$persona->setCedula($this->getRequestParameter('cedula'));
			$persona->setTipoDoc($this->getRequestParameter('tipo_identificacion'));
			$persona->setNombre($this->getRequestParameter('nombre'));
			$persona->setApellido($this->getRequestParameter('apellido'));
			$persona->setSexo($this->getRequestParameter('sexo'));
			$persona->setTelefonoFijo($this->getRequestParameter('telefono'));
			$persona->setTelefonoCelular($this->getRequestParameter('celular'));
			$persona->setEmail($this->getRequestParameter('email'));
			$persona->setUsuario($usuario);

			if ($tipo_usuario!="Admin")
			{
                		$perfil = new Perfil();
    			
    				$perfil->setPersona($persona);
    			}
              
              
			try
			{

			if ($tipo_usuario!="Admin")
			{
			$perfil->save();
			}
			else $persona->save();

			}
			catch (Exception $excepcionUsuario)
			{
			/* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada');
			if($exeptionErrorUsuario > 0)
			{*/
			$salida = "({success: false, errors: { reason: 'Ya existe una cuenta con el mismo nombre de usuario'}})";
			return $salida;
			// }
			}
			  $salida = "({success: true, mensaje:'El usuario fue creado exitosamente'})";
		} 
	
		else 
		{
			  $salida = "({success: false, errors: { reason: 'Ya existe un usuario con ese mismo No. de c&eacute;dula'}})";
		}
    		


    		  
	  return $salida;
	  
    }
	
	
	protected function  updateUsuario()
    {
		$idPersona = $this->getRequestParameter('cedula');
	    $criterio   =   new Criteria();
		$criterio->add(PersonaPeer::CEDULA, $idPersona);
		$persona  = PersonaPeer::doSelectOne($criterio);
		$salida	='';
		if($persona)
		{
			 
			//$usuario->setCedula($this->getRequestParameter('cedula'));
			$persona->setTipoDoc($this->getRequestParameter('tipo_identificacion'));
			$persona->setNombre($this->getRequestParameter('nombre'));
			$persona->setApellido($this->getRequestParameter('apellido'));
			$persona->setSexo($this->getRequestParameter('sexo'));
			$persona->setTelefonoFijo($this->getRequestParameter('telefono'));
			$persona->setTelefonoCelular($this->getRequestParameter('celular'));
			$persona->setEmail($this->getRequestParameter('email'));

			$usuario=$persona->getUsuario();

			$usuario->setLogin($this->getRequestParameter('login'));
			$usuario->setClave(base64_encode ($this->getRequestParameter('clave')));
			$usuario->setTipoUsuario($this->getRequestParameter('tipo_usuario'));
			$usuario->setEstado($this->getRequestParameter('estado'));
        try
        {
          $persona->save();
        }
        catch (Exception $excepcionUsuario)
        {
         /* $exeptionErrorUsuario = substr_count($excepcionUsuario->getMessage(), 'llave duplicada viola restricci�n de unicidad �apis_usuario_login_unique�');
          if($exeptionErrorUsuario > 0)
          {*/
            $salida = "({success: false, errors: { reason: 'Ya existe un usuario con el nombre de usuario igual a: ".$this->getRequestParameter('Login')."'}})";
            return $salida;
          //}
        }
			  $salida = "({success: true, mensaje:'El usuario fue actualizado exitosamente'})";
		  } else {
			  $salida = "({success: false, errors: { reason: 'El usuario no existe con ese No. de c�dula'}})";
		  }
		  return $salida;


	}	
    
	protected function  deleteUsuario()
    {
		$idPersonas = json_decode($this->getRequestParameter('ids'));
		
			//	echo '"cc: '.$idUsuarios[0].'"';
		//	print_r($idPersonas);
  //   return sfView::NONE;		
			$criteria = new Criteria();
			$criteria->add(PersonaPeer::CEDULA, $idPersonas, Criteria::IN);
			$criteria->addJoin(PerfilPeer::PERSONA_ID,PersonaPeer::ID_PERSONA);
			
			
		   $perfiles  = PerfilPeer::doSelect($criteria);
		
				
    // return sfView::NONE;		

	//	$salida = "({success: true, mensaje:'El usuario "+	$usuario->getNombre()+" "+$usuario->getApellido()+"fue borrado exitosamente'})";

        
        $nombre="";
        $apellido="";	
        
	
    		foreach ($perfiles As $temporal)
    		{
    		
    	//	print_r($usuario);

                $persona=$temporal->getPersona();
                
                $nombre=$persona->getNombre();
                $apellido=$persona->getApellido();
                $usuario=$persona->getUsuario();
                
                
                
                $temporal->delete();
                
                $persona->delete();
                
                $usuario->delete();
                
                $salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";       
                   
            } 
                if( count($idPersonas)<2)
                
                		  $salida = "({success: true, mensaje:'El usuario $nombre $apellido fue borrado exitosamente'})";
                	//$salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
                 else 
                    $salida = "({success: true, mensaje:'Los usuarios fueron borrados exitosamente'})";
                 
          
        
		  return $salida;


	}   
}
