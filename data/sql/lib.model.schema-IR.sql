
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- sipel_usuario
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_usuario`;


CREATE TABLE `sipel_usuario`
(
	`id_usuario` INTEGER  NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(20)  NOT NULL,
	`clave` TEXT  NOT NULL,
	`tipo_usuario` VARCHAR(30)  NOT NULL COMMENT 'Admin, Monitor, Egresado, Empresa',
	`estado` VARCHAR(1)  NOT NULL COMMENT '1-Habilitado,2-Deshabilitado',
	PRIMARY KEY (`id_usuario`),
	UNIQUE KEY `sipel_usuario_U_1` (`login`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- sipel_persona
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_persona`;


CREATE TABLE `sipel_persona`
(
	`id_persona` INTEGER  NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER,
	`cedula` VARCHAR(10)  NOT NULL,
	`tipo_doc` VARCHAR(50)  NOT NULL COMMENT 'Cedula de Ciudadania,Documento de Identidad, Cedula de Extranjeria,NIT',
	`lugar_expedicion` VARCHAR(50),
	`nombre` VARCHAR(50)  NOT NULL,
	`apellido` VARCHAR(50)  NOT NULL,
	`sexo` VARCHAR(1)  NOT NULL COMMENT 'f-Femenino,m-Masculino',
	`telefono_fijo` VARCHAR(50)  NOT NULL,
	`telefono_celular` VARCHAR(50),
	`direccion` VARCHAR(50),
	`email` VARCHAR(50),
	`fecha_nacimiento` DATE,
	`pais_origen` VARCHAR(50),
	`departamento` VARCHAR(50),
	`ciudad` VARCHAR(50),
	PRIMARY KEY (`id_persona`),
	INDEX `sipel_persona_FI_1` (`usuario_id`),
	CONSTRAINT `sipel_persona_FK_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `sipel_usuario` (`id_usuario`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- sipel_empresa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_empresa`;


CREATE TABLE `sipel_empresa`
(
	`id_empresa` INTEGER  NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER,
	`persona_id` INTEGER COMMENT 'Persona de la empresa encargada del mantenimiento de la cuenta ',
	`nit` VARCHAR(10)  NOT NULL,
	`nombre_empresa` VARCHAR(50)  NOT NULL,
	`telefono_fijo` VARCHAR(50)  NOT NULL,
	`direccion` VARCHAR(50)  NOT NULL,
	`email` VARCHAR(50)  NOT NULL,
	`departamento` VARCHAR(50)  NOT NULL,
	`ciudad` VARCHAR(50)  NOT NULL,
	PRIMARY KEY (`id_empresa`),
	INDEX `sipel_empresa_FI_1` (`usuario_id`),
	CONSTRAINT `sipel_empresa_FK_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `sipel_usuario` (`id_usuario`),
	INDEX `sipel_empresa_FI_2` (`persona_id`),
	CONSTRAINT `sipel_empresa_FK_2`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- sipel_perfil
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_perfil`;


CREATE TABLE `sipel_perfil`
(
	`id_perfil` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER,
	`nivel_academico` VARCHAR(50)  NOT NULL,
	`area_estudio` VARCHAR(50)  NOT NULL,
	`estado_estudio` VARCHAR(12)  NOT NULL COMMENT 'Terminado,Estudiando,Suspendido',
	`estado` VARCHAR(1)  NOT NULL COMMENT '1-Empleado,2-Desempleado',
	`idiomas` TEXT  NOT NULL,
	`becas` VARCHAR(50)  NOT NULL,
	`resumen` TEXT  NOT NULL COMMENT 'URL donde esta alojada la Hoja de Vida',
	`url_file` TEXT,
    FULLTEXT(`idiomas`,`resumen`),
	PRIMARY KEY (`id_perfil`),
	INDEX `sipel_perfil_FI_1` (`persona_id`),
	CONSTRAINT `sipel_perfil_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
);

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
