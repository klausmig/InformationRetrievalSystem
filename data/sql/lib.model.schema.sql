
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- sipel_empresa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_empresa`;


CREATE TABLE `sipel_empresa`
(
	`id_empresa` INTEGER  NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER,
	`persona_id` INTEGER COMMENT 'Persona de la empresa encargada del mantenimiento de la cuenta ',
	`nit` VARCHAR(10)  NOT NULL,
	`nombre_empresa` VARCHAR(50)  NOT NULL,
	`telefono_fijo` VARCHAR(50)  NOT NULL,
	`direccion` VARCHAR(50)  NOT NULL,
	`email` VARCHAR(50)  NOT NULL,
	`departamento` VARCHAR(50)  NOT NULL,
	`ciudad` VARCHAR(50)  NOT NULL,
	PRIMARY KEY (`id_empresa`),
	INDEX `sipel_empresa_FI_1` (`usuario_id`),
	CONSTRAINT `sipel_empresa_FK_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `sipel_usuario` (`id_usuario`),
	INDEX `sipel_empresa_FI_2` (`persona_id`),
	CONSTRAINT `sipel_empresa_FK_2`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_usuario
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_usuario`;


CREATE TABLE `sipel_usuario`
(
	`id_usuario` INTEGER  NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(20)  NOT NULL,
	`clave` TEXT  NOT NULL,
	`tipo_usuario` VARCHAR(30)  NOT NULL COMMENT 'Admin, Monitor, Egresado, Empresa',
	`estado` VARCHAR(1)  NOT NULL COMMENT '1-Habilitado,2-Deshabilitado',
	PRIMARY KEY (`id_usuario`),
	UNIQUE KEY `sipel_usuario_U_1` (`login`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_persona
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_persona`;


CREATE TABLE `sipel_persona`
(
	`id_persona` INTEGER  NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER,
	`cedula` VARCHAR(10)  NOT NULL,
	`tipo_doc` VARCHAR(50)  NOT NULL COMMENT 'Cedula de Ciudadania,Documento de Identidad, Cedula de Extranjeria,NIT',
	`lugar_expedicion` VARCHAR(50),
	`nombre` VARCHAR(50)  NOT NULL,
	`apellido` VARCHAR(50)  NOT NULL,
	`sexo` VARCHAR(1)  NOT NULL COMMENT 'F-Femenino,M-Masculino',
	`estado_civil` VARCHAR(10) COMMENT 'SOLTERO,CASADO',
	`telefono_fijo` VARCHAR(50)  NOT NULL,
	`telefono_celular` VARCHAR(50),
	`direccion` VARCHAR(50),
	`email` VARCHAR(50),
	`fecha_nacimiento` DATE,
	`pais_origen` VARCHAR(50),
	`departamento` VARCHAR(50),
	`ciudad` VARCHAR(50),
	PRIMARY KEY (`id_persona`),
	INDEX `sipel_persona_FI_1` (`usuario_id`),
	CONSTRAINT `sipel_persona_FK_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `sipel_usuario` (`id_usuario`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_perfil
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_perfil`;


CREATE TABLE `sipel_perfil`
(
	`id_perfil` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`nivel_academico` VARCHAR(50) COMMENT 'Doctorado, Maestria, Postgrado, Pregrado, Tecnologo',
	`area_estudio` VARCHAR(50),
	`estado_estudio` VARCHAR(12) COMMENT 'Terminado,Estudiando,Suspendido',
	`promedio_acumulado` VARCHAR(5),
	`puntaje_ecaes` VARCHAR(50),
	`semestre_cursando` VARCHAR(50),
	`estado_perfil` VARCHAR(12) COMMENT 'Empleado,Desempleado',
	`idiomas` TEXT,
	`becas` VARCHAR(50),
	`resumen` TEXT,
	`url_file` TEXT COMMENT 'URL donde esta alojada la Hoja de Vida',
	PRIMARY KEY (`id_perfil`),
	INDEX `sipel_perfil_FI_1` (`persona_id`),
	CONSTRAINT `sipel_perfil_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_idioma
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_idioma`;


CREATE TABLE `sipel_idioma`
(
	`id_idioma` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`idioma` VARCHAR(50),
	`habla` VARCHAR(50),
	`escribe` VARCHAR(50),
	`lee` VARCHAR(50),
	PRIMARY KEY (`id_idioma`),
	INDEX `sipel_idioma_FI_1` (`persona_id`),
	CONSTRAINT `sipel_idioma_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_estudio
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_estudio`;


CREATE TABLE `sipel_estudio`
(
	`id_estudio` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`numero` INTEGER,
	`institucion` VARCHAR(50),
	`lugar` VARCHAR(50),
	`anno` VARCHAR(12),
	`tipo_estudio` VARCHAR(50) COMMENT 'Doctorado, Maestria, Postgrado, Pregrado, Tecnologo',
	`titulo` TEXT,
	PRIMARY KEY (`id_estudio`),
	INDEX `sipel_estudio_FI_1` (`persona_id`),
	CONSTRAINT `sipel_estudio_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_capacitacion
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_capacitacion`;


CREATE TABLE `sipel_capacitacion`
(
	`id_capacitacion` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`numero` INTEGER,
	`institucion` VARCHAR(50),
	`anno` VARCHAR(12),
	`capacitacion` TEXT,
	PRIMARY KEY (`id_capacitacion`),
	INDEX `sipel_capacitacion_FI_1` (`persona_id`),
	CONSTRAINT `sipel_capacitacion_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_experiencia
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_experiencia`;


CREATE TABLE `sipel_experiencia`
(
	`id_experiencia` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`numero` INTEGER,
	`institucion` VARCHAR(50),
	`fecha_inicio` DATE,
	`fecha_fin` DATE,
	`logro` TEXT,
	`responsabilidad` TEXT,
	PRIMARY KEY (`id_experiencia`),
	INDEX `sipel_experiencia_FI_1` (`persona_id`),
	CONSTRAINT `sipel_experiencia_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

#-----------------------------------------------------------------------------
#-- sipel_articulo
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sipel_articulo`;


CREATE TABLE `sipel_articulo`
(
	`id_articulo` INTEGER  NOT NULL AUTO_INCREMENT,
	`persona_id` INTEGER  NOT NULL,
	`numero` INTEGER,
	`titulo` VARCHAR(50),
	`anno` VARCHAR(12),
	`articulo` TEXT,
	PRIMARY KEY (`id_articulo`),
	INDEX `sipel_articulo_FI_1` (`persona_id`),
	CONSTRAINT `sipel_articulo_FK_1`
		FOREIGN KEY (`persona_id`)
		REFERENCES `sipel_persona` (`id_persona`)
)Type=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
